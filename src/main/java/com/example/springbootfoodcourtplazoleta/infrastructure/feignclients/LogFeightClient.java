package com.example.springbootfoodcourtplazoleta.infrastructure.feignclients;

import com.example.springbootfoodcourtplazoleta.domain.model.LogRequest;
import com.example.springbootfoodcourtplazoleta.domain.model.LogResponse;
import com.example.springbootfoodcourtplazoleta.domain.spi.LogPersistencePort;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "foodcourt-traceability", url = "http://localhost:8090")
public interface LogFeightClient extends LogPersistencePort {
    @PostMapping("/log")
    void saveLog(@RequestBody LogRequest logRequest);

    @GetMapping("/log")
    List<LogResponse> getLogs(@RequestParam Long idCustomer, @RequestParam Long idOrder);
}
