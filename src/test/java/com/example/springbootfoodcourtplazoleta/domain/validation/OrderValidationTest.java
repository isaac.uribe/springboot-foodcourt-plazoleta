package com.example.springbootfoodcourtplazoleta.domain.validation;

import com.example.springbootfoodcourtplazoleta.domain.exception.EmptyField;
import com.example.springbootfoodcourtplazoleta.domain.exception.IncorrectRol;
import com.example.springbootfoodcourtplazoleta.domain.exception.ListEmpty;
import com.example.springbootfoodcourtplazoleta.domain.model.OrderDetail;
import com.example.springbootfoodcourtplazoleta.domain.model.RankingEmployee;
import com.example.springbootfoodcourtplazoleta.domain.model.Restaurant;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class OrderValidationTest {
    OrderValidation orderValidation = new OrderValidation();
    @Test
    void validateRestaurant() {
        Restaurant restaurant = new Restaurant();
        Restaurant restaurantNull = null;
        assertDoesNotThrow(()->orderValidation.validateRestaurant(restaurant));
        assertThrows(EmptyField.class, ()->orderValidation.validateRestaurant(restaurantNull));
    }
    @Test
    void validateOrderDetail() {
        List<OrderDetail> orderDetails = new ArrayList<>();
        orderDetails.add(new OrderDetail());
        List<OrderDetail> orderDetailsNull = null;
        assertDoesNotThrow(()->orderValidation.validateOrderDetail(orderDetails));
        assertThrows(ListEmpty.class, ()->orderValidation.validateOrderDetail(orderDetailsNull));
    }

    @Test
    void validateEmail() {
        String email = "ejemplo@gmail.com";
        String emailNull = null;
        assertDoesNotThrow(()->orderValidation.validateEmail(email));
        assertThrows(EmptyField.class, ()->orderValidation.validateEmail(emailNull));
    }

    @Test
    void validateRestaurantId() {
        Long id = 1L;
        Long idNull = null;
        assertDoesNotThrow(()->orderValidation.validateRestaurantId(id));
        assertThrows(EmptyField.class, ()->orderValidation.validateRestaurantId(idNull));
    }
    @Test
    void validateRolCustomer(){
        String rol = "CUSTOMER";
        String rolWrong = "admin";
        assertDoesNotThrow(()->orderValidation.validateRolCustomer(rol));
        assertThrows(IncorrectRol.class, ()->orderValidation.validateRolCustomer(rolWrong));
    }
    @Test
    void validateRolEmployee(){
        String rol = "EMPLOYEE";
        String rolWrong = "admin";
        assertDoesNotThrow(()->orderValidation.validateRolEmployee(rol));
        assertThrows(IncorrectRol.class,()->orderValidation.validateRolEmployee(rolWrong));
    }

}