package com.example.springbootfoodcourtplazoleta.application.dto;

import com.example.springbootfoodcourtplazoleta.domain.model.Category;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DishResponse {
    private String name;
    private String description;
    private Category category;
    private Integer price;
    private String urlImg;
}
