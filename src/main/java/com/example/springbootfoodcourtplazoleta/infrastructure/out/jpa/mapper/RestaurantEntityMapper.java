package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.mapper;

import com.example.springbootfoodcourtplazoleta.domain.model.Restaurant;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.RestaurantEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface RestaurantEntityMapper {
    RestaurantEntity toRestaurantEntity(Restaurant restaurant);
    Restaurant toRestaurant(RestaurantEntity restaurantEntity);
    List<Restaurant> toRestaurantList(List<RestaurantEntity> restaurantEntities);
}
