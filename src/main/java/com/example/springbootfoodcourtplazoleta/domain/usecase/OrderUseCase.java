package com.example.springbootfoodcourtplazoleta.domain.usecase;

import com.example.springbootfoodcourtplazoleta.domain.api.OrderServicePort;
import com.example.springbootfoodcourtplazoleta.domain.exception.EmailNotExist;
import com.example.springbootfoodcourtplazoleta.domain.exception.IdNotExist;
import com.example.springbootfoodcourtplazoleta.domain.model.*;
import com.example.springbootfoodcourtplazoleta.domain.spi.OrderPersistencePort;
import com.example.springbootfoodcourtplazoleta.domain.spi.UserPersistencePort;
import com.example.springbootfoodcourtplazoleta.domain.validation.OrderValidation;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.StatusOfOrder;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class OrderUseCase implements OrderServicePort {

    private final OrderPersistencePort orderPersistencePort;
    private final UserPersistencePort userPersistencePort;
    private final OrderValidation orderValidation;

    public OrderUseCase(OrderPersistencePort orderPersistencePort, UserPersistencePort userPersistencePort, OrderValidation orderValidation) {
        this.orderPersistencePort = orderPersistencePort;
        this.userPersistencePort = userPersistencePort;
        this.orderValidation = orderValidation;
    }
    @Override
    public void saveOrder(Order order) {
        orderValidation.validateRestaurant(order.getRestaurant());
        orderValidation.validateOrderDetail(order.getDetails());
        orderValidation.validateEmail(order.getEmailCustomer());
        orderValidation.validateRestaurantId(order.getRestaurant().getId());

        try {
            UserAuth userAuth = userPersistencePort.getUserByEmail(order.getEmailCustomer());
            orderValidation.validateRolCustomer(userAuth.getRol().getRol().name());
            order.setIdCustomer(userAuth.getId());
        }catch (Exception e){
            throw new EmailNotExist();
        }

        order.setDate(LocalDateTime.now());
        orderPersistencePort.saveOrder(order);
    }

    @Override
    public List<Order> getOrdersPaginatedStatus(Integer numberOfRecords, String status, Long idRestaurant) {
        return orderPersistencePort.getOrdersPaginatedStatus(numberOfRecords, status, idRestaurant);
    }

    @Override
    public Order assignedAnOrder(Long id, Order order) {
        orderValidation.validateRestaurantId(order.getIdChef());

        try {
            User user = userPersistencePort.getUser(order.getIdChef());
            orderValidation.validateRolEmployee(user.getRol().getRol().name());
        }catch (Exception e){
            throw new IdNotExist();
        }
        order.setStatus(StatusOfOrder.IN_PREPARATION.name());
        return orderPersistencePort.assignedAnOrder(id, order);
    }
    @Override
    public Order getOrder(Long id) {
        return orderPersistencePort.getOrder(id);
    }

    @Override
    public void sendingSms(Long idCustomer, Long idEmployee) {
        orderPersistencePort.sendingSms(idCustomer, idEmployee);
    }

    @Override
    public void toDelivery(Long idCustomer, Long idEmployee, Integer code) {
        orderPersistencePort.toDelivery(idCustomer, idEmployee, code);
    }

    @Override
    public void cancelOrder(Long idOrder, String email) {
        orderPersistencePort.cancelOrder(idOrder, email);
    }

    @Override
    public List<OrderTime> getTimesOrder() {
        return orderPersistencePort.getTimesOrder();
    }

    @Override
    public List<RankingEmployee> getRankingEmployeeOrder() {
        return orderPersistencePort.getRankingEmployeeOrder();
    }


}
