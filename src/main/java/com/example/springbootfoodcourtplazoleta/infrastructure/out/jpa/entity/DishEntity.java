package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "dish")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DishEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    private Integer price;
    private String urlImg;
    private Boolean status = true;
    @ManyToOne
    @JoinColumn(name = "idCategory")
    private CategoryEntity category;
    @ManyToOne
    @JoinColumn(name = "idRestaurant")
    private RestaurantEntity restaurant;
}
