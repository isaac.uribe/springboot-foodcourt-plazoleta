package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity;

import com.example.springbootfoodcourtplazoleta.domain.model.OrderDetail;
import com.example.springbootfoodcourtplazoleta.domain.model.Restaurant;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
@Entity
@Table(name = "order_entity")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class OrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long idCustomer;
    private LocalDateTime date;
    @Enumerated(EnumType.STRING)
    private StatusOfOrder status = StatusOfOrder.PENDING;
    private Long idChef;
    @ManyToOne
    @JoinColumn(name = "idRestaurant")
    private RestaurantEntity restaurant;
    @Transient
    private String emailCustomer;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "IdOrderDetail")
    private List<OrderDetailEntity> details;

    private LocalDateTime dateEnd;
    @Transient
    private Duration timeOrder;
}
