package com.example.springbootfoodcourtplazoleta.domain.usecase;

import com.example.springbootfoodcourtplazoleta.domain.exception.IdNotExist;
import com.example.springbootfoodcourtplazoleta.domain.model.Restaurant;
import com.example.springbootfoodcourtplazoleta.domain.model.Rol;
import com.example.springbootfoodcourtplazoleta.domain.model.Role;
import com.example.springbootfoodcourtplazoleta.domain.model.User;
import com.example.springbootfoodcourtplazoleta.domain.spi.RestaurantPersistencePort;
import com.example.springbootfoodcourtplazoleta.domain.spi.UserPersistencePort;
import com.example.springbootfoodcourtplazoleta.domain.validation.RestaurantValidation;
import feign.FeignException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static com.example.springbootfoodcourtplazoleta.domain.model.Role.ADMIN;
import static com.example.springbootfoodcourtplazoleta.domain.model.Role.OWNER;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
@SpringBootTest
class RestaurantUseCaseTest {
    private Restaurant restaurant;
    private User user;
    private Rol rol;

    @InjectMocks RestaurantUseCase restaurantUseCase;

    @Mock
    RestaurantPersistencePort restaurantPersistencePort;

    @Mock
    RestaurantValidation restaurantValidation;

    @Mock
    UserPersistencePort userPersistencePort;

    @BeforeEach
    void setUp() {
        Long id = 1L;;
        String description = "ejemplo de descripcion";
        rol = new Rol(id, OWNER, description);

        String name = "nombre";
        String lastname = "apellido";
        String cellPhone = "+123465";
        String email = "ejemplo@gmail.com";
        user = new User(name, lastname, cellPhone, email, rol);

        String addres = "calle 26";
        String urlLogo = "Ejemplo de url";
        String nit = "132164";
        restaurant = new Restaurant(id, name, addres, cellPhone, urlLogo, nit, id);


    }

    @Test
    void saveRestaurant() {
       when(userPersistencePort.getUser(1L)).thenReturn(user);
       restaurantUseCase.saveRestaurant(restaurant);
       verify(userPersistencePort, times(1)).getUser(1L);
    }

    @Test
    void saveRestaurantWithError(){
        when(userPersistencePort.getUser(anyLong())).thenThrow(FeignException.BadRequest.class);
        assertThrows(IdNotExist.class, ()->restaurantUseCase.saveRestaurant(restaurant));
    }

    @Test
    void gelAllRestaurant() {
        List<Restaurant> restaurantList = new ArrayList<>();
        restaurantList.add(restaurant);

        when(restaurantPersistencePort.gelAllRestaurant()).thenReturn(restaurantList);
        List<Restaurant> restaurants = restaurantUseCase.gelAllRestaurant();
        verify(restaurantPersistencePort, times(1)).gelAllRestaurant();
    }

    @Test
    void getRestaurant() {
        when(restaurantPersistencePort.getRestaurant(1L)).thenReturn(restaurant);
        Restaurant result = restaurantUseCase.getRestaurant(1L);
        assertEquals(result, restaurant);

    }
    @Test
    void getAllRestaurantPaginated(){
        List<Restaurant> restaurantList = new ArrayList<>();
        restaurantList.add(restaurant);
        when(restaurantPersistencePort.getAllRestaurantPaginated(1)).thenReturn(restaurantList);
        List<Restaurant> restaurants = restaurantUseCase.getAllRestaurantPaginated(1);
        verify(restaurantPersistencePort, times(1)).getAllRestaurantPaginated(1);
    }
}