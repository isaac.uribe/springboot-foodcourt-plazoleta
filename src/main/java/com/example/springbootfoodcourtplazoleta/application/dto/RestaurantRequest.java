package com.example.springbootfoodcourtplazoleta.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestaurantRequest {
    private String name;
    private String address;
    private String phone;
    private String urlLogo;
    private String nit;
    private Long idUser;

}
