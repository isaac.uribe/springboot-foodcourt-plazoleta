package com.example.springbootfoodcourtplazoleta.domain.model;

import java.time.LocalDateTime;

public class OrderTime {
    private Long id;
    private String statusOrder;
    private Long idChef;
    private LocalDateTime dateStar;
    private LocalDateTime dateEnd;
    private Long hour;
    private Long minutes;
    private Long seconds;

    public OrderTime() {
    }

    public OrderTime(Long id, String statusOrder, Long idChef, LocalDateTime dateStar, LocalDateTime dateEnd, Long hour, Long minutes, Long seconds) {
        this.id = id;
        this.statusOrder = statusOrder;
        this.idChef = idChef;
        this.dateStar = dateStar;
        this.dateEnd = dateEnd;
        this.hour = hour;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatusOrder() {
        return statusOrder;
    }

    public void setStatusOrder(String statusOrder) {
        this.statusOrder = statusOrder;
    }

    public Long getIdChef() {
        return idChef;
    }

    public void setIdChef(Long idChef) {
        this.idChef = idChef;
    }

    public Long getHour() {
        return hour;
    }

    public void setHour(Long hour) {
        this.hour = hour;
    }

    public Long getMinutes() {
        return minutes;
    }

    public void setMinutes(Long minutes) {
        this.minutes = minutes;
    }

    public Long getSeconds() {
        return seconds;
    }

    public void setSeconds(Long seconds) {
        this.seconds = seconds;
    }

    public LocalDateTime getDateStar() {
        return dateStar;
    }

    public void setDateStar(LocalDateTime dateStar) {
        this.dateStar = dateStar;
    }

    public LocalDateTime getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(LocalDateTime dateEnd) {
        this.dateEnd = dateEnd;
    }
}
