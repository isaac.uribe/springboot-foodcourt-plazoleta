package com.example.springbootfoodcourtplazoleta.infrastructure.exception;

public class InfrasUserNotFound extends RuntimeException{
    public InfrasUserNotFound() {
        super();
    }
}
