package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.adapter;

import com.example.springbootfoodcourtplazoleta.domain.model.Category;
import com.example.springbootfoodcourtplazoleta.domain.model.Dish;
import com.example.springbootfoodcourtplazoleta.domain.model.Restaurant;
import com.example.springbootfoodcourtplazoleta.infrastructure.exception.IdNoPresent;
import com.example.springbootfoodcourtplazoleta.infrastructure.exception.PaginatonNotNull;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.CategoryEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.DishEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.RestaurantEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.mapper.DishEntityMapper;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository.CategoryRespository;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository.DishRepository;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository.RestaurantRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestExecutionListeners;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class DishJpaAdapterTest {
    private Dish dish;
    private Restaurant restaurant;
    private Category category;
    private RestaurantEntity restaurantEntity;
    private CategoryEntity categoryEntity;

    private DishEntity dishEntity;
    @InjectMocks
    DishJpaAdapter dishJpaAdapter;

    @Mock
    DishRepository dishRepository;
    @Mock
    DishEntityMapper dishEntityMapper;
    @Mock
    RestaurantRepository restaurantRepository;
    @Mock
    CategoryRespository categoryRespository;
    @BeforeEach
    void setUp() {
        dish = new Dish();
        category = new Category();
        category.setId(1L);
        dish.setCategory(category);
        restaurant = new Restaurant();
        restaurant.setId(1L);
        dish.setRestaurant(restaurant);
         categoryEntity = new CategoryEntity();
        categoryEntity.setId(1L);

         restaurantEntity = new RestaurantEntity();
        restaurantEntity.setId(1L);



    }
    @Test
    void saveDish() {
        Dish dish = new Dish();
        Category category = new Category();
        category.setId(1L);
        dish.setCategory(category);
        Restaurant restaurant = new Restaurant();
        restaurant.setId(1L);
        dish.setRestaurant(restaurant);
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setId(1L);

        RestaurantEntity restaurantEntity = new RestaurantEntity();
        restaurantEntity.setId(1L);

        when(categoryRespository.findById(dish.getCategory().getId())).thenReturn(Optional.of(categoryEntity));
        when(restaurantRepository.findById(dish.getRestaurant().getId())).thenReturn(Optional.of(restaurantEntity));
        DishEntity dishEntity = new DishEntity();
        when(dishEntityMapper.toDishEntity(dish)).thenReturn(dishEntity);

        dishJpaAdapter.saveDish(dish);
        verify(categoryRespository).findById(1L);
        verify(restaurantRepository).findById(1L);
        verify(dishEntityMapper).toDishEntity(dish);
        verify(dishRepository).save(dishEntity);
    }
    @Test
    void saveDishIdNotExistCategory(){
        when(categoryRespository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(IdNoPresent.class, ()->dishJpaAdapter.saveDish(dish));
    }
    @Test
    void saveDishIdNotExistRestaurant(){
        when(categoryRespository.findById(1L)).thenReturn(Optional.of(categoryEntity));
        when(restaurantRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(IdNoPresent.class, ()->dishJpaAdapter.saveDish(dish));
    }

    @Test
    void getAllDish() {
        List<DishEntity> dishEntityList = new ArrayList<>();
        dishEntityList.add(new DishEntity());
        when(dishRepository.findAll()).thenReturn(dishEntityList);
        List<Dish> result = dishJpaAdapter.getAllDish();
        assertEquals(result, dishEntityMapper.toDishList(dishEntityList));
    }

    @Test
    void getDish() {
        when(dishRepository.findById(1L)).thenReturn(Optional.of(new DishEntity()));
        Dish result = dishJpaAdapter.getDish(1L);
        assertEquals(result, dishEntityMapper.toDish(new DishEntity()));
    }

    @Test
    void getDishIdNotExist(){
        when(dishRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(IdNoPresent.class,()->dishJpaAdapter.getDish(1L));
    }
    @Test
    void updateDish(){
        Long dishId = 1L;
        Dish dish = new Dish();
        dish.setPrice(10);
        dish.setDescription("ejemplo de descripcion");
        when(dishRepository.findById(1L)).thenReturn(Optional.of(new DishEntity()));
        Dish result = dishJpaAdapter.updateDish(dishId, dish);
        verify(dishRepository, times(1)).save(any());
    }
    @Test
    void updateDishIdNotExist(){
        when(dishRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(IdNoPresent.class,()->dishJpaAdapter.updateDish(1L,new Dish()));
    }
    @Test
    void changueStatus(){
        Long dishId = 1L;
        Dish dish = new Dish();
        dish.setPrice(10);
        dish.setDescription("ejemplo de descripcion");
        when(dishRepository.findById(1L)).thenReturn(Optional.of(new DishEntity()));
        Dish result = dishJpaAdapter.changeStatus(dishId, dish);
        verify(dishRepository, times(1)).save(any());
    }
    @Test
    void changueStatusIdNotExist(){
        when(dishRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(IdNoPresent.class, ()->dishJpaAdapter.changeStatus(1L, new Dish()));
    }
    @Test
    void getDishesRestaurantCategory() {
        Long idRestaurant = 1L;
        Long idCategory = 2L;
        int numberOfRecords = 10;
        Pageable pagerList = PageRequest.of(0, numberOfRecords);

        RestaurantEntity restaurantEntity = new RestaurantEntity();
        restaurantEntity.setId(idRestaurant);

        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setId(idCategory);

        when(restaurantRepository.findById(idRestaurant)).thenReturn(Optional.of(restaurantEntity));
        when(categoryRespository.findById(idCategory)).thenReturn(Optional.of(categoryEntity));

        List<DishEntity> dishEntities = new ArrayList<>();


        Page<DishEntity> dishEntityPage = new PageImpl<>(dishEntities, pagerList, dishEntities.size());
        when(dishRepository.findByRestaurantAndCategoryAndStatus(restaurantEntity, categoryEntity,true, pagerList)).thenReturn(dishEntityPage);


        List<Dish> expectedDishes = new ArrayList<>();

        when(dishEntityMapper.toDish(any(DishEntity.class))).thenAnswer(invocation -> {
            List<Dish> expectedDishes2 = new ArrayList<>();


            expectedDishes2.add(new Dish());
            expectedDishes2.add(new Dish());

            return expectedDishes2;
        });


        List<Dish> result = dishJpaAdapter.getDishsRestaurantCategory(numberOfRecords, idRestaurant, idCategory);


        assertNotNull(result);
    }
    @Test
    void getDishesRestaurantCategoryWithError(){
        int numberOfRecords = 0;
        assertThrows(PaginatonNotNull.class,()->dishJpaAdapter.getDishsRestaurantCategory(numberOfRecords, 1L, 1L));
    }
}