package com.example.springbootfoodcourtplazoleta.domain.usecase;

import com.example.springbootfoodcourtplazoleta.domain.api.RestauranteServicePort;
import com.example.springbootfoodcourtplazoleta.domain.exception.IdNotExist;
import com.example.springbootfoodcourtplazoleta.domain.model.Restaurant;
import com.example.springbootfoodcourtplazoleta.domain.model.User;
import com.example.springbootfoodcourtplazoleta.domain.spi.RestaurantPersistencePort;
import com.example.springbootfoodcourtplazoleta.domain.spi.UserPersistencePort;
import com.example.springbootfoodcourtplazoleta.domain.validation.RestaurantValidation;
import feign.FeignException;
import org.springframework.data.domain.Page;


import java.util.List;

public class RestaurantUseCase implements RestauranteServicePort {

    private final RestaurantPersistencePort restaurantPersistencePort;
    private final UserPersistencePort userPersistencePort;
    private final RestaurantValidation restaurantValidation;

    public RestaurantUseCase(RestaurantPersistencePort restaurantPersistencePort, UserPersistencePort userPersistencePort, RestaurantValidation restaurantValidation) {
        this.restaurantPersistencePort = restaurantPersistencePort;
        this.userPersistencePort = userPersistencePort;
        this.restaurantValidation = restaurantValidation;
    }

    @Override
    public void saveRestaurant(Restaurant restaurant) {
        restaurantValidation.validateId(restaurant.getIdUser());
        try {
            User user =  userPersistencePort.getUser(restaurant.getIdUser());
            restaurantValidation.validateRol(user.getRol().getRol().name());

        }catch (FeignException.BadRequest badRequest){
            throw new IdNotExist();
        }

        restaurantValidation.validateNit(restaurant.getNit());
        restaurantValidation.validatePhone(restaurant.getPhone());
        restaurantValidation.validateName(restaurant.getName());
        restaurantValidation.validateEmptyField(restaurant.getAddress());
        restaurantValidation.validateEmptyField(restaurant.getUrlLogo());
        restaurantPersistencePort.saveRestaurant(restaurant);
    }

    @Override
    public List<Restaurant> gelAllRestaurant() {
        return restaurantPersistencePort.gelAllRestaurant();
    }

    @Override
    public Restaurant getRestaurant(Long id) {

        return restaurantPersistencePort.getRestaurant(id);
    }

    @Override
    public List<Restaurant> getAllRestaurantPaginated(Integer numberOfRecords) {
        return restaurantPersistencePort.getAllRestaurantPaginated(numberOfRecords);
    }
}

