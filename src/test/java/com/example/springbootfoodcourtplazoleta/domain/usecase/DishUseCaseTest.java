package com.example.springbootfoodcourtplazoleta.domain.usecase;

import com.example.springbootfoodcourtplazoleta.domain.exception.NoPermissionForUpdate;
import com.example.springbootfoodcourtplazoleta.domain.model.*;
import com.example.springbootfoodcourtplazoleta.domain.spi.DishPersistencePort;
import com.example.springbootfoodcourtplazoleta.domain.spi.UserPersistencePort;
import com.example.springbootfoodcourtplazoleta.domain.validation.DishValidation;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.CategoryEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.RestaurantEntity;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

import static com.example.springbootfoodcourtplazoleta.domain.model.Role.OWNER;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class DishUseCaseTest {
    private Dish dish;
    private Restaurant restaurant;
    private Category category;
    private User user;
    private Rol rol;
    @InjectMocks
    DishUseCase dishUseCase;
    @Mock
    DishPersistencePort dishPersistencePort;
    @Mock
    UserPersistencePort userPersistencePort;
    @Mock
    HttpSession session;
    @Mock
    DishValidation dishValidation;
    @BeforeEach
    void setUp() {

        Long idRestaurant = 1L;
        String name = "ejemplo de nombre";
        String address = "ejemplo de direccion";
        Long idPropietario = 2L;
        String phone = "+23423423";
        String urlLogo = "ejemplo de url";
        String nit = "32432432";
        restaurant = new Restaurant(idRestaurant,name,address,phone,urlLogo,nit,idPropietario);
        Long idCategory = 1L;
        String description = "ejemplo de descripcion";
        category = new Category(idCategory, name, description);
        Long idDish = 1L;
        Integer price = 100;
        Boolean status = true;
        dish = new Dish(idDish,name,category,description,price,restaurant,urlLogo,status);
        String lastName = "ejemplo de apellido";
        String email = "ejemplo@gmail.com";
        user = new User(name, lastName, phone, email, new Rol());
    }

    @Test
    void saveDish() {
        dishUseCase.saveDish(dish);
        verify(dishPersistencePort, times(1)).saveDish(dish);
    }

    @Test
    void getAllDish() {
        List<Dish> dishList = dishUseCase.getAllDish();
        verify(dishPersistencePort, times(1)).getAllDish();
    }

    @Test
    void getDish() {
        when(dishPersistencePort.getDish(1L)).thenReturn(dish);
        Dish result = dishUseCase.getDish(1L);
        assertEquals(result, dish);
    }
    @Test
    void updateDish(){
        when(userPersistencePort.getUser(2L)).thenReturn(user);
        when(session.getAttribute("userEmail")).thenReturn(user.getEmail());
        when(dishUseCase.updateDish(1L, dish)).thenReturn(dish);
        Dish result = dishUseCase.updateDish(1L, dish);
    }
    @Test
    void upadteDishNoPermission(){
        when(userPersistencePort.getUser(2L)).thenReturn(user);
        assertThrows(NoPermissionForUpdate.class, ()->dishUseCase.updateDish(1L, dish));
    }
    @Test
    void changueStatus(){
        when(userPersistencePort.getUser(2L)).thenReturn(user);
        when(session.getAttribute("userEmail")).thenReturn(user.getEmail());
        when(dishUseCase.updateDish(1L, dish)).thenReturn(dish);
        Dish result = dishUseCase.changeStatus(1L, dish);
    }
    @Test
    void changueStatusNoPermission(){
        when(userPersistencePort.getUser(2L)).thenReturn(user);
        assertThrows(NoPermissionForUpdate.class, ()->dishUseCase.changeStatus(1L, dish));
    }
    @Test
    void getDishesRestaurantCategory(){
        List<Dish> dishList = new ArrayList<>();
        dishList.add(dish);
        when(dishPersistencePort.getDishsRestaurantCategory(1,1L, 1L)).thenReturn(dishList);
        List<Dish> dishes = dishUseCase.getDishsRestaurantCategory(1, 1L, 1L);
        verify(dishPersistencePort, times(1)).getDishsRestaurantCategory(1,1L,1L);
    }
}