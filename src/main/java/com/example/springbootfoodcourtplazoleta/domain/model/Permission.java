package com.example.springbootfoodcourtplazoleta.domain.model;

public enum Permission {
    ADMIN_CREATE_RESTAURANT("admin:createrestaurant"),
    ADMIN_READ_ALL("admin:readall"),
    ADMIN_READ_ONE("admin:readone"),
    OWNER_CREATE_DISH("owner:createdish"),
    OWNER_READ_ALL("owner:readalldish"),
    OWNER_READ_ONE("owner:readonedish"),
    OWNER_UPDATE_DISH("owner:updatedish"),
    EMPLOYEE_READ_ORDER_PENDING("employee:readorderpending"),
    CUSTOMER_READ_ALL_RESTAURANT("customer:readallrestaurant");

    Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }

    private final String permission;
}
