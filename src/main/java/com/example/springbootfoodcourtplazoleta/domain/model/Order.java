package com.example.springbootfoodcourtplazoleta.domain.model;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

public class Order {
    private Long id;
    private Long idCustomer;
    private LocalDateTime date;
    private String status;
    private Long idChef;
    private Restaurant restaurant;
    private String emailCustomer;
    private List<OrderDetail> details;
    private LocalDateTime dateEnd;

    private Duration timeOrder;
    public Order() {
    }

    public Order(Long id, Long idCustomer, LocalDateTime date, String status, Long idChef, Restaurant restaurant, String emailCustomer, List<OrderDetail> details, LocalDateTime dateEnd, Duration timeOrder) {
        this.id = id;
        this.idCustomer = idCustomer;
        this.date = date;
        this.status = status;
        this.idChef = idChef;
        this.restaurant = restaurant;
        this.emailCustomer = emailCustomer;
        this.details = details;
        this.dateEnd = dateEnd;
        this.timeOrder = timeOrder;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Long idCustomer) {
        this.idCustomer = idCustomer;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getIdChef() {
        return idChef;
    }

    public void setIdChef(Long idChef) {
        this.idChef = idChef;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public String getEmailCustomer() {
        return emailCustomer;
    }

    public void setEmailCustomer(String emailCustomer) {
        this.emailCustomer = emailCustomer;
    }

    public List<OrderDetail> getDetails() {
        return details;
    }

    public void setDetails(List<OrderDetail> details) {
        this.details = details;
    }

    public LocalDateTime getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(LocalDateTime dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Duration getTimeOrder() {
        return timeOrder;
    }

    public void setTimeOrder(Duration timeOrder) {
        this.timeOrder = timeOrder;
    }
}
