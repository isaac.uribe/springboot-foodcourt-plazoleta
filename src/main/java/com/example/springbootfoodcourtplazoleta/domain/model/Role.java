package com.example.springbootfoodcourtplazoleta.domain.model;

import org.springframework.security.core.authority.SimpleGrantedAuthority;


import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.example.springbootfoodcourtplazoleta.domain.model.Permission.*;

public enum Role {

    USER(Collections.emptySet()),

    ADMIN(
            Set.of(
                    ADMIN_CREATE_RESTAURANT,
                    ADMIN_READ_ALL,
                    ADMIN_READ_ONE
            )
    ),
    OWNER(
            Set.of(
                    OWNER_CREATE_DISH,
                    OWNER_READ_ALL,
                    OWNER_READ_ONE,
                    OWNER_UPDATE_DISH
            )
    ),
    EMPLOYEE(
            Set.of(
                    EMPLOYEE_READ_ORDER_PENDING
            )
    ),
    CUSTOMER(
            Set.of(
                    CUSTOMER_READ_ALL_RESTAURANT
            )
    );
    Role(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    private Set<Permission> permissions;

    public List<SimpleGrantedAuthority> getAuthorities(){
        var authorities = getPermissions()
                .stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toList());
        authorities.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return authorities;
    }
}
