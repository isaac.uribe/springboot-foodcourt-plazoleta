package com.example.springbootfoodcourtplazoleta.domain.spi;

import com.example.springbootfoodcourtplazoleta.domain.model.LogRequest;
import com.example.springbootfoodcourtplazoleta.domain.model.LogResponse;
import lombok.extern.java.Log;

import java.util.List;

public interface LogPersistencePort {
    void saveLog(LogRequest logRequest);
    List<LogResponse> getLogs(Long idCustomer, Long idOrder);
}
