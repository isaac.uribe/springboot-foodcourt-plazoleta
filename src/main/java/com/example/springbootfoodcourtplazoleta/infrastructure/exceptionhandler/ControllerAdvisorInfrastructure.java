package com.example.springbootfoodcourtplazoleta.infrastructure.exceptionhandler;

import com.example.springbootfoodcourtplazoleta.infrastructure.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Collections;
import java.util.Map;

@ControllerAdvice
public class ControllerAdvisorInfrastructure {
    private static final String MESSAGE = "Message";
    @ExceptionHandler(IdNoPresent.class)
    public ResponseEntity<Map<String, String>> idNotExist(IdNoPresent idNotExist){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.ID_NO_PRESENT.getMessage()));
    }
    @ExceptionHandler(InfrasUserNotFound.class)
    public ResponseEntity<Map<String,String>> userNotFound(InfrasUserNotFound userNotFound){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.USER_NOT_FOUND.getMessage()));
    }
    @ExceptionHandler(PaginatonNotNull.class)
    public  ResponseEntity<Map<String,String>> paginationNotNull(PaginatonNotNull paginatonNotNull){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.PAGINATION_NOT_NULL.getMessage()));
    }
    @ExceptionHandler(OrderInProcess.class)
    public ResponseEntity<Map<String,String>> orderInProcess(OrderInProcess orderInProcess){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.ORDER_IN_PROCESS.getMessage()));
    }
    @ExceptionHandler(StatusNotExist.class)
    public ResponseEntity<Map<String, String>> statusNotExist(StatusNotExist statusNotExist){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.STATUS_NOT_EXIST.getMessage()));
    }
    @ExceptionHandler(OrderInPreparationNotExist.class)
    public ResponseEntity<Map<String, String>> statusNotExist(OrderInPreparationNotExist orderInPreparationNotExist){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.ORDER_IN_PREPARATION_NOT_EXIST.getMessage()));
    }
    @ExceptionHandler(OrderReadyNotExist.class)
    public ResponseEntity<Map<String, String>> orderNotExist(OrderReadyNotExist orderReadyNotExist){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.ORDER_READY_NOT_EXIST.getMessage()));
    }
    @ExceptionHandler(NotEqual.class)
    public ResponseEntity<Map<String, String>> notEqual(NotEqual notEqual){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.NOT_EQUAL.getMessage()));
    }
    @ExceptionHandler(OrderCustomerWrong.class)
    public ResponseEntity<Map<String,String>> orderCustomerWrong(OrderCustomerWrong orderCustomerWrong){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.ORDER_CUSTOMER_WRONG.getMessage()));
    }
    @ExceptionHandler(OrderInPreparation.class)
    public ResponseEntity<Map<String, String>> ordeInPreparation(OrderInPreparation order){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.ORDER_IN_PREPARATION.getMessage()));
    }
    @ExceptionHandler(OrderHasIdChef.class)
    public ResponseEntity<Map<String,String>> orderHasIdChef(OrderHasIdChef orderHasIdChef){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.ORDER_HAS_ID_CHEF.getMessage()));
    }
}
