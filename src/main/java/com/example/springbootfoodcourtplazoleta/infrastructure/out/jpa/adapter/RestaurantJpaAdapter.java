package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.adapter;

import com.example.springbootfoodcourtplazoleta.domain.model.Restaurant;
import com.example.springbootfoodcourtplazoleta.domain.spi.RestaurantPersistencePort;
import com.example.springbootfoodcourtplazoleta.infrastructure.exception.IdNoPresent;
import com.example.springbootfoodcourtplazoleta.infrastructure.exception.PaginatonNotNull;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.RestaurantEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.mapper.RestaurantEntityMapper;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository.RestaurantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;


import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class RestaurantJpaAdapter implements RestaurantPersistencePort {
    private final RestaurantRepository restaurantRepository;
    private final RestaurantEntityMapper restaurantEntityMapper;
    @Override
    public void saveRestaurant(Restaurant restaurant) {
        restaurantRepository.save(restaurantEntityMapper.toRestaurantEntity(restaurant));
    }
    @Override
    public List<Restaurant> gelAllRestaurant() {
        List<RestaurantEntity> restaurantEntityList = restaurantRepository.findAll();
        return restaurantEntityMapper.toRestaurantList(restaurantEntityList);
    }
    @Override
    public Restaurant getRestaurant(Long id) {
        Optional<RestaurantEntity> restaurantEntityOptional = restaurantRepository.findById(id);
        if (!restaurantEntityOptional.isPresent()){
            throw new IdNoPresent();
        }
        RestaurantEntity restaurantEntity = restaurantEntityOptional.get();
        return restaurantEntityMapper.toRestaurant(restaurantEntity);
    }

    @Override
    public List<Restaurant> getAllRestaurantPaginated(Integer numberOfRecords) {
        try {
            Pageable pagerList = PageRequest.of(0,numberOfRecords);
            Page<RestaurantEntity> restaurantEntityPage = restaurantRepository.findAll(pagerList);
            Page<Restaurant> restaurants = restaurantEntityPage.map(restaurantEntity -> restaurantEntityMapper.toRestaurant(restaurantEntity));
            return restaurants.getContent();
        }catch (Exception e){
            throw new PaginatonNotNull();
        }
    }
}
