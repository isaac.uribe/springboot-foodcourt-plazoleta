package com.example.springbootfoodcourtplazoleta.application.mapper;

import com.example.springbootfoodcourtplazoleta.application.dto.DishRequestChangueStatus;
import com.example.springbootfoodcourtplazoleta.domain.model.Dish;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(
        componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface DishRequestChangueStatusMapper {
    Dish toDishChangueStatus(DishRequestChangueStatus dishRequestChangueStatus);
}
