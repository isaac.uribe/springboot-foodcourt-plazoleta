package com.example.springbootfoodcourtplazoleta.domain.api;

import com.example.springbootfoodcourtplazoleta.domain.model.Restaurant;
import org.springframework.data.domain.Page;

import java.util.List;

public interface RestauranteServicePort {
    void saveRestaurant(Restaurant restaurant);
    List<Restaurant> gelAllRestaurant();
    Restaurant getRestaurant(Long id);

    List<Restaurant> getAllRestaurantPaginated(Integer numberOfRecords);

}
