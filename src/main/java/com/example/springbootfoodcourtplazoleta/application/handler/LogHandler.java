package com.example.springbootfoodcourtplazoleta.application.handler;

import com.example.springbootfoodcourtplazoleta.domain.model.LogResponse;

import java.util.List;

public interface LogHandler {
    List<LogResponse> getLogs(Long idCustomer, Long idOrder);
}
