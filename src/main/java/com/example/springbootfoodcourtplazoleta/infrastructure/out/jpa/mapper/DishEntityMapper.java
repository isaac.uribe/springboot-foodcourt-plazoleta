package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.mapper;

import com.example.springbootfoodcourtplazoleta.domain.model.Dish;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.DishEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(
        componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface DishEntityMapper {
    DishEntity toDishEntity(Dish dish);
    List<Dish> toDishList(List<DishEntity> dishEntityList);
    Dish toDish(DishEntity dishEntity);
}
