package com.example.springbootfoodcourtplazoleta.application.handler;

import com.example.springbootfoodcourtplazoleta.domain.api.LogServicePort;
import com.example.springbootfoodcourtplazoleta.domain.model.LogResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class LogHandlerImplement implements LogHandler{
    private final LogServicePort logServicePort;

    @Override
    public List<LogResponse> getLogs(Long idCustomer, Long idOrder) {
        return logServicePort.getLogs(idCustomer, idOrder);
    }
}
