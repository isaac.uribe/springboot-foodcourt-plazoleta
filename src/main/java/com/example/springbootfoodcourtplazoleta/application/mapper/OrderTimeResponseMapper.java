package com.example.springbootfoodcourtplazoleta.application.mapper;

import com.example.springbootfoodcourtplazoleta.application.dto.OrderTimeResponse;
import com.example.springbootfoodcourtplazoleta.domain.model.Order;
import com.example.springbootfoodcourtplazoleta.domain.model.OrderTime;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(
        componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface OrderTimeResponseMapper {
    OrderTimeResponse toOrderTimeResponse(OrderTime orderTime);
    List<OrderTimeResponse> toOrderTimeResponseList(List<OrderTime> orderTimes);
}
