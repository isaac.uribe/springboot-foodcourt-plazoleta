package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.adapter;

import com.example.springbootfoodcourtplazoleta.domain.exception.IdNotExist;
import com.example.springbootfoodcourtplazoleta.domain.model.Restaurant;
import com.example.springbootfoodcourtplazoleta.infrastructure.exception.IdNoPresent;
import com.example.springbootfoodcourtplazoleta.infrastructure.exception.PaginatonNotNull;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.RestaurantEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.mapper.RestaurantEntityMapper;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository.RestaurantRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class RestaurantJpaAdapterTest {
    private Restaurant restaurant;
    @InjectMocks
    RestaurantJpaAdapter restaurantJpaAdapter;

    @Mock
    RestaurantRepository restaurantRepository;
    @Mock
    RestaurantEntityMapper restaurantEntityMapper;
    @BeforeEach
    void setUp() {
    }

    @Test
    void saveRestaurant() {
        restaurantJpaAdapter.saveRestaurant(new Restaurant());
        verify(restaurantRepository, times(1)).save(any());
    }

    @Test
    void gelAllRestaurant() {
        List<RestaurantEntity> restaurantEntityList = new ArrayList<>();
        restaurantEntityList.add(new RestaurantEntity());
        when(restaurantRepository.findAll()).thenReturn(restaurantEntityList);
        List<Restaurant> restaurants = restaurantJpaAdapter.gelAllRestaurant();
        assertEquals(restaurants, restaurantEntityMapper.toRestaurantList(restaurantEntityList));
    }

    @Test
    void getRestaurant() {
        when(restaurantRepository.findById(1L)).thenReturn(Optional.of(new RestaurantEntity()));
        Restaurant result = restaurantJpaAdapter.getRestaurant(1L);
        assertEquals(result, restaurantEntityMapper.toRestaurant(new RestaurantEntity()));
    }

    @Test
    void getRestaurantIdNotExist(){
        when(restaurantRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(IdNoPresent.class, ()->restaurantJpaAdapter.getRestaurant(1L));
    }
    @Test
    void getAllRestaurantPaginated(){
        int numberOfRecords = 10;
        Pageable pagerList = PageRequest.of(0, numberOfRecords);


        Page<RestaurantEntity> restaurantEntityPage = mock(Page.class);
        when(restaurantRepository.findAll(pagerList)).thenReturn(restaurantEntityPage);


        Page<Restaurant> expectedRestaurants = mock(Page.class);
        when(restaurantEntityMapper.toRestaurant(Mockito.any(RestaurantEntity.class))).thenReturn(Mockito.mock(Restaurant.class));
        when(restaurantEntityPage.map(Mockito.any())).thenReturn((Page)expectedRestaurants);
        List<Restaurant> actualRestaurants = restaurantJpaAdapter.getAllRestaurantPaginated(numberOfRecords);

        assertEquals(expectedRestaurants.getContent(), actualRestaurants);
    }
    @Test
    void getAllRestaurantPaginatedWithError(){
        int numberOfRecords = 0;
        assertThrows(PaginatonNotNull.class, ()->restaurantJpaAdapter.getAllRestaurantPaginated(numberOfRecords));
    }
}