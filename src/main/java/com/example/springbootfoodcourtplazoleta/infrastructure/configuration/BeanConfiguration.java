package com.example.springbootfoodcourtplazoleta.infrastructure.configuration;

import com.example.springbootfoodcourtplazoleta.domain.api.DishServicePort;
import com.example.springbootfoodcourtplazoleta.domain.api.LogServicePort;
import com.example.springbootfoodcourtplazoleta.domain.api.OrderServicePort;
import com.example.springbootfoodcourtplazoleta.domain.api.RestauranteServicePort;
import com.example.springbootfoodcourtplazoleta.domain.spi.*;
import com.example.springbootfoodcourtplazoleta.domain.usecase.DishUseCase;
import com.example.springbootfoodcourtplazoleta.domain.usecase.LogUseCase;
import com.example.springbootfoodcourtplazoleta.domain.usecase.OrderUseCase;
import com.example.springbootfoodcourtplazoleta.domain.usecase.RestaurantUseCase;
import com.example.springbootfoodcourtplazoleta.domain.validation.DishValidation;
import com.example.springbootfoodcourtplazoleta.domain.validation.OrderValidation;
import com.example.springbootfoodcourtplazoleta.domain.validation.RestaurantValidation;
import com.example.springbootfoodcourtplazoleta.infrastructure.feignclients.LogFeightClient;
import com.example.springbootfoodcourtplazoleta.infrastructure.feignclients.UserFeightClient;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.adapter.DishJpaAdapter;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.adapter.LogAdapter;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.adapter.OrderJpaAdapter;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.adapter.RestaurantJpaAdapter;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.mapper.DishEntityMapper;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.mapper.OrderEntityMapper;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.mapper.RestaurantEntityMapper;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository.CategoryRespository;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository.DishRepository;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository.OrderRepository;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository.RestaurantRepository;
import com.example.springbootfoodcourtplazoleta.infrastructure.twilio.TwilioConfiguration;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {
    private final RestaurantRepository restaurantRepository;
    private final RestaurantEntityMapper restaurantEntityMapper;
    private final UserFeightClient userFeightClient;
    private final RestaurantValidation restaurantValidation;
    private final DishRepository dishRepository;
    private final DishEntityMapper dishEntityMapper;
    private final CategoryRespository categoryRespository;
    private final DishValidation dishValidation;
    private final HttpSession session;
    private final OrderRepository orderRepository;
    private final OrderEntityMapper orderEntityMapper;
    private final OrderValidation orderValidation;
    private final TwilioConfiguration twilioConfiguration;
    private final LogFeightClient logFeightClient;

    @Bean
    public RestaurantPersistencePort restaurantPersistencePort(){
        return new RestaurantJpaAdapter(restaurantRepository, restaurantEntityMapper);
    }
    @Bean
    public RestauranteServicePort restauranteServicePort(){
        return new RestaurantUseCase(restaurantPersistencePort(),userFeightClient, restaurantValidation);
    }
    @Bean
    public DishPersistencePort dishPersistencePort(){
        return new DishJpaAdapter(dishRepository, dishEntityMapper, restaurantRepository, categoryRespository);
    }
    @Bean
    public DishServicePort dishServicePort(){
        return new DishUseCase(dishPersistencePort(), dishValidation, userFeightClient, session);
    }
    @Bean
    public OrderPersistencePort orderPersistencePort(){
        return new OrderJpaAdapter(orderRepository, orderEntityMapper, restaurantRepository, userFeightClient, twilioConfiguration, logFeightClient);
    }
    @Bean
    public OrderServicePort orderServicePort(){
        return new OrderUseCase(orderPersistencePort(), userFeightClient, orderValidation);
    }
    @Bean
    public LogPersistencePortForGet logPersistencePortForGet(){
        return new LogAdapter(logFeightClient);
    }
    @Bean
    public LogServicePort logServicePort(){
        return new LogUseCase(logPersistencePortForGet());
    }

}
