package com.example.springbootfoodcourtplazoleta.application.mapper;

import com.example.springbootfoodcourtplazoleta.application.dto.RankingEmployeeResponse;
import com.example.springbootfoodcourtplazoleta.domain.model.RankingEmployee;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface RankingResponseMapper {
    RankingEmployeeResponse toRankingResponse(RankingEmployee rankingEmployee);
    List<RankingEmployeeResponse> toRankingResponseList(List<RankingEmployee> rankingEmployees);
}
