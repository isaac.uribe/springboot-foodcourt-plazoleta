package com.example.springbootfoodcourtplazoleta.application.mapper;

import com.example.springbootfoodcourtplazoleta.application.dto.DishResponsePaginated;
import com.example.springbootfoodcourtplazoleta.domain.model.Dish;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface DishResponsePaginatedMapper {
    List<DishResponsePaginated> toDishResponsePaginated(List<Dish> dishList);
}
