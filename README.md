<br />
<div align="center">
<h3 align="center">Spring Boot FoodCourt Plazoleta</h3>
<p>
This is a backend project that is in charge of managing the restaurants, orders and dishes of a square
</p>
</div>

### Build With
* ![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white)
* ![Spring](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
* ![Gradle](https://img.shields.io/badge/Gradle-02303A.svg?style=for-the-badge&logo=Gradle&logoColor=white)
* ![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white)

### Getting Started

To get a local copy up and running follow these steps.

### Prerequisited
* JDK 17
* Gradle
* Postgrest

### Installation

1. Clone the Repo
2. Created a new database in POSTGREST called foodcourtplazoleta
3. Update the database connection settings
   ```yml
   # src/main/resources/application.yml   
   spring: 
      datasource:
        url: jdbc:postgresql://localhost:5432/foodcourtplazoleta
        username:
        password: 
   ```
   
## Usage
1. Open [http://localhost:8085/swagger-ui/index.html#/](http://localhost:8085/swagger-ui/index.html#/)