package com.example.springbootfoodcourtplazoleta.application.mapper;

import com.example.springbootfoodcourtplazoleta.application.dto.DishResponse;
import com.example.springbootfoodcourtplazoleta.domain.model.Dish;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(
        componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface DishResponseMapper {
    DishResponse toDishResponse(Dish dish);
    List<DishResponse> toDishResponseList(List<Dish> dishList);
}
