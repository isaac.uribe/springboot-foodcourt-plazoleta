package com.example.springbootfoodcourtplazoleta.domain.spi;

import com.example.springbootfoodcourtplazoleta.domain.model.User;
import com.example.springbootfoodcourtplazoleta.domain.model.UserAuth;

public interface UserPersistencePort{
    User getUser(Long id);
    UserAuth getUserByEmail(String email);
}
