package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity;

public enum StatusOfOrder {
    PENDING("Pendiente"),
    IN_PREPARATION("En preparacion"),
    READY("Listo"),
    DELIVERED("Entregado"),
    CANCELLED("Cancelado");

    private final String description;

    StatusOfOrder(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
