package com.example.springbootfoodcourtplazoleta.domain.validation;

import com.example.springbootfoodcourtplazoleta.domain.exception.EmptyField;
import com.example.springbootfoodcourtplazoleta.domain.exception.IncorrectRol;
import com.example.springbootfoodcourtplazoleta.domain.exception.NegativePrice;
import com.example.springbootfoodcourtplazoleta.domain.model.Category;
import com.example.springbootfoodcourtplazoleta.domain.model.Restaurant;
import org.springframework.stereotype.Component;

@Component
public class DishValidation {
    public void validateEmptyField(String data){
        if(data == null){
            throw new EmptyField();
        }
    }
    public void validatePriceDish(Integer price){
        if (price == null || price<0){
            throw new NegativePrice();
        }
    }
    public void validateRestaurant(Restaurant restaurant){
        if (restaurant == null){
            throw new EmptyField();
        }
    }
    public void validateCategory(Category category){
        if (category == null){
            throw new EmptyField();
        }
    }
    public void validateStatus(Boolean status){
        if (status == null){
            throw new EmptyField();
        }
    }
}
