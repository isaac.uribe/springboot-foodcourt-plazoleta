package com.example.springbootfoodcourtplazoleta.application.handler;

import com.example.springbootfoodcourtplazoleta.application.dto.*;
import com.example.springbootfoodcourtplazoleta.application.mapper.*;
import com.example.springbootfoodcourtplazoleta.domain.api.OrderServicePort;
import com.example.springbootfoodcourtplazoleta.domain.model.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderHandlerImplement implements OrderHandler{
    private final OrderServicePort orderServicePort;
    private final OrderRequestMapper orderRequestMapper;
    private final OrderResponseMapper orderResponseMapper;
    private final OrderRequestAssignedMapper orderRequestAssignedMapper;
    private final OrderTimeResponseMapper orderTimeResponseMapper;
    private final RankingResponseMapper rankingResponseMapper;
    @Override
    public void saveOrder(OrderRequest orderRequest) {
        Order order = orderRequestMapper.toOrder(orderRequest);
        orderServicePort.saveOrder(order);
    }

    @Override
    public List<OrderResponse> getOrdersPaginatedStatus(Integer numberOfRecords, String status, Long idRestaurant) {
        List<Order> orders = orderServicePort.getOrdersPaginatedStatus(numberOfRecords, status, idRestaurant);
        return orderResponseMapper.toOrderResponseList(orders);
    }

    @Override
    public OrderResponse assignedAnOrder(Long id, OrderRequestAssigned orderRequestAssigned) {
        Order oldOrder = orderServicePort.getOrder(id);
        Order newOrder = orderRequestAssignedMapper.toOrder(orderRequestAssigned);
        oldOrder.setIdChef(newOrder.getIdChef());
        orderServicePort.assignedAnOrder(id, oldOrder);
        return orderResponseMapper.toOrderResponse(oldOrder);
    }

    @Override
    public void sendingSms(Long idCustomer, Long idEmployee) {
        orderServicePort.sendingSms(idCustomer, idEmployee);
    }

    @Override
    public void toDelivery(Long idCustomer, Long idEmployee, Integer code) {
        orderServicePort.toDelivery(idCustomer, idEmployee, code);
    }

    @Override
    public void cancelOrder(Long idOrder, String email) {
        orderServicePort.cancelOrder(idOrder, email);
    }

    @Override
    public List<OrderTimeResponse> getTimesOrder() {
        return orderTimeResponseMapper.toOrderTimeResponseList(orderServicePort.getTimesOrder());
    }

    @Override
    public List<RankingEmployeeResponse> getRankingEmployeeOrder() {
        return rankingResponseMapper.toRankingResponseList(orderServicePort.getRankingEmployeeOrder());
    }

}
