package com.example.springbootfoodcourtplazoleta.application.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DishRequestChangueStatus {
    private Boolean status;
}
