package com.example.springbootfoodcourtplazoleta.domain.exceptionhandler;

public enum ExceptionResponse {

    INCORRECT_ROL("El usuario asignado no tiene el rol correcto"),

    FIEL_ONLY_NUMERIC("Ingresaste letras o simbolos extraños en los campo nit o telefono sobrepasa la cantidad de carateres permitida"),

    FIEL_CONTAIN_ONLY_NUMERIC("El campo nombre puede contener numeros pero no puede ser solo numeros o es null"),

    EMPTY_FIELD("Hay un campo campo vacio por favor revise"),
    ID_NOT_EXIST("El id no existe o el rol del usuario es incorrecto"),
    NEGATIVE_PRICE("El precio no puede ser negativo o esta vacio"),
    NO_PERMISSION_FOR_UPDATE("No puedes moficar un plato en un restaurante que no eres dueño"),
    LIST_DISH_EMPTY("La lista de platos no puedo ser nula o estar vacia"),
    EMAIL_NOT_EXIST("El correo ingresado no existe");

    private String message;

    ExceptionResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
