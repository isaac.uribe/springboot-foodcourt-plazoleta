package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository;

import com.example.springbootfoodcourtplazoleta.application.dto.OrderResponse;
import com.example.springbootfoodcourtplazoleta.domain.model.Order;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.OrderEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.RestaurantEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.StatusOfOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
    List<OrderEntity> findByIdCustomerAndStatusIn(Long idCustomer, List<StatusOfOrder> statusList);

    List<OrderEntity> findByStatus(StatusOfOrder status);
    List<OrderEntity> findByIdChef(Long idChef);
    Page<OrderEntity> findByStatusAndRestaurant(StatusOfOrder status, RestaurantEntity restaurant, Pageable pagerList);
    Optional<OrderEntity> findByStatusAndIdCustomerAndIdChef(StatusOfOrder status, Long idCustomer, Long idChef);
}
