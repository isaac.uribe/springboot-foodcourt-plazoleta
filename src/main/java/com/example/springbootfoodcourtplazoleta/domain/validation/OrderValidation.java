package com.example.springbootfoodcourtplazoleta.domain.validation;

import com.example.springbootfoodcourtplazoleta.domain.exception.EmptyField;
import com.example.springbootfoodcourtplazoleta.domain.exception.IncorrectRol;
import com.example.springbootfoodcourtplazoleta.domain.exception.ListEmpty;
import com.example.springbootfoodcourtplazoleta.domain.model.OrderDetail;
import com.example.springbootfoodcourtplazoleta.domain.model.Restaurant;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class OrderValidation {
    public void validateRestaurant(Restaurant restaurant){
        if (restaurant == null){
            throw new EmptyField();
        }
    }
    public void validateOrderDetail(List<OrderDetail> orderDetails){
        if (orderDetails == null || orderDetails.isEmpty()){
            throw new ListEmpty();
        }
    }
    public void validateEmail(String email){
        if (email == null){
            throw new EmptyField();
        }
    }
    public void  validateRestaurantId(Long id){
        if (id == null){
            throw new EmptyField();
        }
    }
    public void validateRolCustomer(String data) {
        if (data == null || !data.equals("CUSTOMER")) {
            throw new IncorrectRol();
        }
    }
    public void validateRolEmployee(String data) {
        if (data == null || !data.equals("EMPLOYEE")) {
            throw new IncorrectRol();
        }
    }
}
