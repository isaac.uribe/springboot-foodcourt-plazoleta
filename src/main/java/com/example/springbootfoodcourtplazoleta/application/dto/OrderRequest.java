package com.example.springbootfoodcourtplazoleta.application.dto;

import com.example.springbootfoodcourtplazoleta.domain.model.OrderDetail;
import com.example.springbootfoodcourtplazoleta.domain.model.Restaurant;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class OrderRequest {
    private String emailCustomer;
    private Restaurant restaurant;
    private List<OrderDetail> details;
}
