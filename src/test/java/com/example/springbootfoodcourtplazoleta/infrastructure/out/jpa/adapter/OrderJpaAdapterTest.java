package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.adapter;

import com.example.springbootfoodcourtplazoleta.domain.exception.EmailNotExist;
import com.example.springbootfoodcourtplazoleta.domain.model.*;
import com.example.springbootfoodcourtplazoleta.infrastructure.exception.*;
import com.example.springbootfoodcourtplazoleta.infrastructure.feignclients.LogFeightClient;
import com.example.springbootfoodcourtplazoleta.infrastructure.feignclients.UserFeightClient;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.OrderEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.RestaurantEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.StatusOfOrder;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.mapper.OrderEntityMapper;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository.OrderRepository;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository.RestaurantRepository;
import com.example.springbootfoodcourtplazoleta.infrastructure.twilio.TwilioConfiguration;
import com.twilio.rest.api.v2010.account.MessageCreator;

import jakarta.persistence.criteria.CriteriaBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import com.twilio.rest.api.v2010.account.Message;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@SpringBootTest
class OrderJpaAdapterTest {

    Order order;
    @InjectMocks
    OrderJpaAdapter orderJpaAdapter;
    @Mock
    OrderRepository orderRepository;
    @Mock
    OrderEntityMapper orderEntityMapper;
    @Mock
    RestaurantRepository restaurantRepository;
    @Mock
    LogFeightClient logFeightClient;
    @Mock
    UserFeightClient userFeightClient;
    @Mock
    TwilioConfiguration twilioConfiguration;
    @Mock
    MessageCreator messageCreator;
    private Map<String, Integer> codeToPhoneMap;

    @BeforeEach
    void setUp() {
        order = new Order();
        order.setIdCustomer(1L);


    }
    @Test
    void saveOrder() {
        when(orderRepository.findByIdCustomerAndStatusIn(eq(order.getIdCustomer()), anyList())).thenReturn(new ArrayList<>());

        orderJpaAdapter.saveOrder(order);
        verify(orderRepository, times(1)).save(any());
    }
    @Test
    void saveOrderWithError(){
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setStatus(StatusOfOrder.PENDING);
        when(orderRepository.findByIdCustomerAndStatusIn(anyLong(), anyList())).thenReturn(Arrays.asList(orderEntity));
        assertThrows(OrderInProcess.class, ()->orderJpaAdapter.saveOrder(order));
    }
    @Test
    void getOrdersPaginatedStatus(){
        Long idRestaurant = 1L;
        int numberOfRecords = 10;
        order.setStatus("PENDING");
        StatusOfOrder statusOfOrder = StatusOfOrder.PENDING;
        Pageable pagerList = PageRequest.of(0, numberOfRecords);
        RestaurantEntity restaurantEntity = new RestaurantEntity();
        restaurantEntity.setId(idRestaurant);
        when(restaurantRepository.findById(idRestaurant)).thenReturn(Optional.of(restaurantEntity));
        List<OrderEntity> orderEntities = new ArrayList<>();
        Page<OrderEntity> orderEntityPage = new PageImpl<>(orderEntities, pagerList, orderEntities.size());
        when(orderRepository.findByStatusAndRestaurant(statusOfOrder, restaurantEntity, pagerList)).thenReturn(orderEntityPage);

        List<Order> expectedOrders = new ArrayList<>();

        when(orderEntityMapper.toOrder(any(OrderEntity.class))).thenAnswer(invocation -> {
            List<Order> expectedDishes2 = new ArrayList<>();


            expectedDishes2.add(new Order());
            expectedDishes2.add(new Order());

            return expectedDishes2;
        });
        List<Order> result = orderJpaAdapter.getOrdersPaginatedStatus(numberOfRecords, order.getStatus(), idRestaurant);
        assertNotNull(result);
    }
    @Test
    void getOrdersPaginatedStatusWithError(){
        Integer numberOfRecords = 0;
        assertThrows(PaginatonNotNull.class, ()->orderJpaAdapter.getOrdersPaginatedStatus(numberOfRecords, order.getStatus(), 1L));
    }
    @Test
    void getStatusFromStringError(){
        String statusError = "INVALIDED";
        assertThrows(StatusNotExist.class, ()-> orderJpaAdapter.getStatusFromString(statusError));
    }
    @Test
    void assignedAnOrder(){
        Long orderId = 1L;
        Order order = new Order();
        OrderEntity currentOrder = new OrderEntity();
        User userCustomer = new User();
        User userEmployee = new User();

        when(orderRepository.findById(orderId)).thenReturn(Optional.of(currentOrder));
        when(userFeightClient.getUser(currentOrder.getIdCustomer())).thenReturn(userCustomer);
        when(userFeightClient.getUser(order.getIdChef())).thenReturn(userEmployee);

        Order result = orderJpaAdapter.assignedAnOrder(orderId, order);

        assertNotNull(result);
        assertEquals(order, result);

        verify(orderRepository).findById(orderId);

    }
    @Test
    void assignedAnOrderWithError(){
        assertThrows(IdNoPresent.class, ()->orderJpaAdapter.assignedAnOrder(1L, order));
    }
    @Test
    void assignedAnOrderIdChefExist(){
        Long orderId = 1L;
        Order order = new Order();
        OrderEntity currentOrder = new OrderEntity();
        currentOrder.setIdChef(2L);
        User userCustomer = new User();
        User userEmployee = new User();

        when(orderRepository.findById(orderId)).thenReturn(Optional.of(currentOrder));
        when(userFeightClient.getUser(currentOrder.getIdCustomer())).thenReturn(userCustomer);
        when(userFeightClient.getUser(order.getIdChef())).thenReturn(userEmployee);
        assertThrows(OrderHasIdChef.class, ()->orderJpaAdapter.assignedAnOrder(orderId, order));
    }
    @Test
    void getOrder(){
        OrderEntity orderEntity = new OrderEntity();
        when(orderRepository.findById(1L)).thenReturn(Optional.of(orderEntity));
        Order result = orderJpaAdapter.getOrder(1L);
        assertEquals(result, orderEntityMapper.toOrder(new OrderEntity()));
    }
    @Test
    void getOrderWithError(){
        assertThrows(IdNoPresent.class, ()->orderJpaAdapter.getOrder(1L));
    }

    @Test
    void sendingSms(){
        Long idCustomer = 1L;
        Long idEmployee = 2L;

        User user = new User();
        user.setCellPhone("+573123553688");
        User userEmployee = new User();
        OrderEntity currentOrder = new OrderEntity();
        Order order = new Order();
        order.setIdChef(2L);
        when(userFeightClient.getUser(idCustomer)).thenReturn(user);

        when(orderRepository.findByStatusAndIdCustomerAndIdChef(StatusOfOrder.IN_PREPARATION, idCustomer, idEmployee))
                .thenReturn(Optional.of(currentOrder));
        when(userFeightClient.getUser(order.getIdChef())).thenReturn(userEmployee);

        when(twilioConfiguration.getTrialNumber()).thenReturn("+17079837976");

        Message message = mock(Message.class);

        when(messageCreator.create()).thenReturn(message);



        orderJpaAdapter.sendingSms(idCustomer, idEmployee);


        verify(userFeightClient, times(1)).getUser(idCustomer);
        verify(orderRepository, times(1)).findByStatusAndIdCustomerAndIdChef(StatusOfOrder.IN_PREPARATION, idCustomer, idEmployee);
        verify(twilioConfiguration, times(1)).getTrialNumber();

    }
    @Test
    void sendingSmsOrderNotExist(){
        assertThrows(OrderInPreparationNotExist.class, ()->orderJpaAdapter.sendingSms(1L, 1L));
    }
    @Test
    void sendingSmsIdNotExist(){
        when(userFeightClient.getUser(1L)).thenThrow(IdNoPresent.class);
        assertThrows(IdNoPresent.class,()->orderJpaAdapter.sendingSms(1L, 1L));
    }
    @Test
    void toDelivery(){

        Long idCustomer = 1L;
        Long idEmployee = 2L;
        Integer code = 1234;
        User user = new User();
        user.setCellPhone("+573123553688");
        OrderEntity orderEntity = new OrderEntity();

        Map<String, Integer> codeToPhoneMap = new HashMap<>();
        codeToPhoneMap.put(user.getCellPhone(), code);

        when(userFeightClient.getUser(idCustomer)).thenReturn(user);
        when(orderRepository.findByStatusAndIdCustomerAndIdChef(StatusOfOrder.READY, idCustomer, idEmployee)).thenReturn(Optional.of(orderEntity));

        orderJpaAdapter.toDelivery(idCustomer, idEmployee, code);

        assertEquals(StatusOfOrder.DELIVERED, orderEntity.getStatus());

        verify(orderRepository, times(1)).findByStatusAndIdCustomerAndIdChef(StatusOfOrder.READY, idCustomer, idEmployee);

    }
    @Test
    void toDeliveyIdNotExist(){
        when(userFeightClient.getUser(1L)).thenThrow(IdNoPresent.class);
        assertThrows(IdNoPresent.class, ()->orderJpaAdapter.toDelivery(1L, 1L, 1234));
    }
    @Test
    void toDelivaryOrderNotExist(){
        User user = new User();
        user.setCellPhone("1234567");

        when(userFeightClient.getUser(1L)).thenReturn(user);
        assertThrows(OrderReadyNotExist.class, ()->orderJpaAdapter.toDelivery(1L, 2L, 1234));
    }
    @Test
    void toDeliveryNotEqual(){
        codeToPhoneMap = new HashMap<>();
        codeToPhoneMap.put("1234567", 5678);
        Long idCustomer = 18L;
        Long idEmployee = 16L;
        Integer code = 1234;
        User user = new User();
        user.setCellPhone("1234567");

        when(userFeightClient.getUser(idCustomer)).thenReturn(user);
        when(orderRepository.findByStatusAndIdCustomerAndIdChef(StatusOfOrder.READY, idCustomer, idEmployee))
                .thenReturn(Optional.of(new OrderEntity()));

        assertThrows(NotEqual.class, ()->orderJpaAdapter.toDelivery(idCustomer, idEmployee, code));
    }
    @Test
    void cancelOrder(){
        order.setIdCustomer(1L);
        order.setStatus(StatusOfOrder.PENDING.name());
        UserAuth userAuth = new UserAuth();
        userAuth.setId(1L);
        userAuth.setEmail("ejemplo@gmail.com");
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setIdCustomer(1L);
        when(userFeightClient.getUserByEmail(userAuth.getEmail())).thenReturn(userAuth);
        when(orderRepository.findById(1L)).thenReturn(Optional.of(orderEntity));
        orderJpaAdapter.cancelOrder(userAuth.getId(), userAuth.getEmail());
        verify(orderRepository, times(1)).save(any(OrderEntity.class));
    }
    @Test
    void cancelOrderEmailNotExist(){
        when(userFeightClient.getUserByEmail("ejemplo@gmail.com")).thenThrow(EmailNotExist.class);
        assertThrows(EmailNotExist.class, ()->orderJpaAdapter.cancelOrder(1L, "ejemplo@gmail.com"));
    }
    @Test
    void cancelOrderIdNotPresent(){
        assertThrows(IdNoPresent.class, ()->orderJpaAdapter.cancelOrder(1L, "ejemplo@gmail.com"));
    }
    @Test
    void cancelOrderCustomerWrong(){
        order.setIdCustomer(2L);
        UserAuth userAuth = new UserAuth();
        userAuth.setId(1L);
        userAuth.setEmail("ejemplo@gmail.com");
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setIdCustomer(2L);
        when(userFeightClient.getUserByEmail(userAuth.getEmail())).thenReturn(userAuth);
        when(orderRepository.findById(1L)).thenReturn(Optional.of(orderEntity));
        assertThrows(OrderCustomerWrong.class, ()->orderJpaAdapter.cancelOrder(1L, userAuth.getEmail()));
    }
    @Test
    void cancelOrderInPreparation(){
        order.setIdCustomer(1L);
        order.setStatus(StatusOfOrder.IN_PREPARATION.name());
        UserAuth userAuth = new UserAuth();
        userAuth.setId(1L);
        userAuth.setEmail("ejemplo@gmail.com");
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setIdCustomer(1L);
        orderEntity.setStatus(StatusOfOrder.IN_PREPARATION);
        when(userFeightClient.getUserByEmail(userAuth.getEmail())).thenReturn(userAuth);
        when(orderRepository.findById(1L)).thenReturn(Optional.of(orderEntity));
        assertThrows(OrderInPreparation.class,()->orderJpaAdapter.cancelOrder(1L, userAuth.getEmail()));
    }
    @Test
    void getTimesOrder(){
        OrderEntity order1 = new OrderEntity();
        order1.setId(1L);
        order1.setStatus(StatusOfOrder.DELIVERED);
        order1.setIdChef(101L);
        order1.setDate(LocalDateTime.of(2023,8,31,10,0));
        order1.setDateEnd(LocalDateTime.of(2023,8,31,12,0));

        OrderEntity order2 = new OrderEntity();
        order2.setId(2L);
        order2.setStatus(StatusOfOrder.DELIVERED);
        order2.setIdChef(102L);
        order2.setDate(LocalDateTime.of(2023,8,31,11,0));
        order2.setDateEnd(LocalDateTime.of(2023,8,31,12,0));

        when(orderRepository.findByStatus(StatusOfOrder.DELIVERED)).thenReturn(Arrays.asList(order1, order2));
        List<OrderTime> orderTimes = orderJpaAdapter.getTimesOrder();
        assertEquals(2, orderTimes.size());
    }
    @Test
    void getRankingEnployeeOrder(){
        OrderEntity order1 = new OrderEntity();
        order1.setId(1L);
        order1.setIdChef(101L);
        OrderEntity order2 = new OrderEntity();
        order2.setId(2L);
        order2.setIdChef(102L);
        User user1 = new User();
        user1.setName("User1");
        User user2 = new User();
        user2.setName("User2");
        when(orderRepository.findAll()).thenReturn(Arrays.asList(order1, order2));

        when(userFeightClient.getUser(101L)).thenReturn(user1);
        when(userFeightClient.getUser(102L)).thenReturn(user2);
        List<RankingEmployee> ranking = orderJpaAdapter.getRankingEmployeeOrder();

        assertEquals(2, ranking.size());
    }
    @Test
    void calculateTimeEmployee(){
        OrderEntity order1 = new OrderEntity();
        order1.setId(1L);
        order1.setIdChef(101L);
        order1.setDate(LocalDateTime.of(2023,8,31,10,0));
        order1.setDateEnd(LocalDateTime.of(2023,8,31,11,30));

        OrderEntity order2 = new OrderEntity();
        order2.setId(2L);
        order2.setIdChef(101L);
        order2.setDate(LocalDateTime.of(2023,8,31,11,0));
        order2.setDateEnd(LocalDateTime.of(2023,8,31,12,0));
        when(orderRepository.findByIdChef(101L)).thenReturn(Arrays.asList(order1, order2));
        double avarageTime = orderJpaAdapter.calculateTimeEmployee(order1);
        assertEquals(4500.0, avarageTime);
    }
}