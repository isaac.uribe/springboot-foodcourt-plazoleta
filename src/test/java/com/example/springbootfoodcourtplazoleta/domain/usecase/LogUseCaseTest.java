package com.example.springbootfoodcourtplazoleta.domain.usecase;

import com.example.springbootfoodcourtplazoleta.domain.model.LogResponse;
import com.example.springbootfoodcourtplazoleta.domain.spi.LogPersistencePortForGet;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class LogUseCaseTest {
    @Mock
    LogPersistencePortForGet logPersistencePortForGet;
    @InjectMocks
    LogUseCase logUseCase;

    @Test
    void getLogs() {
        List<LogResponse> logResponses1 = logUseCase.getLogs(1L,1L);
        Mockito.verify(logPersistencePortForGet, Mockito.times(1)).getLogs(1L,1L);
    }
}