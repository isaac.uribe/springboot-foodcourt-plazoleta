package com.example.springbootfoodcourtplazoleta.application.handler;

import com.example.springbootfoodcourtplazoleta.application.dto.*;

import com.example.springbootfoodcourtplazoleta.application.mapper.*;
import com.example.springbootfoodcourtplazoleta.domain.api.DishServicePort;
import com.example.springbootfoodcourtplazoleta.domain.model.Dish;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@RequiredArgsConstructor
@Transactional
public class DishHandlerImplemet implements DishHandler{
    private final DishServicePort dishServicePort;
    private final DishRequestMapper dishRequestMapper;
    private final DishResponseMapper dishResponseMapper;
    private final DishRequestUpdateMapper dishRequestUpdateMapper;
    private final DishRequestChangueStatusMapper dishRequestChangueStatusMapper;
    private final DishResponsePaginatedMapper dishResponsePaginatedMapper;

    @Override
    public void saveDish(DishRequest dishRequest) {

        Dish dish = dishRequestMapper.toDish(dishRequest);
        dishServicePort.saveDish(dish);

    }
    @Override
    public List<DishResponse> getAllDish() {
        return dishResponseMapper.toDishResponseList(dishServicePort.getAllDish());
    }

    @Override
    public DishResponse getDish(Long id) {
        return dishResponseMapper.toDishResponse(dishServicePort.getDish(id));
    }

    @Override
    public DishResponse updateDish(Long id, DishRequestForUpdate dishRequestForUpdate) {

        Dish oldDish = dishServicePort.getDish(id);
        Dish newDish = dishRequestUpdateMapper.toDishForUpdate(dishRequestForUpdate);
        oldDish.setPrice(newDish.getPrice());
        oldDish.setDescription(newDish.getDescription());

        dishServicePort.updateDish(id, oldDish);

        return dishResponseMapper.toDishResponse(oldDish);
    }

    @Override
    public DishResponse changueStatus(Long id, DishRequestChangueStatus dishRequestChangueStatus) {
        Dish oldDish = dishServicePort.getDish(id);
        Dish newDish = dishRequestChangueStatusMapper.toDishChangueStatus(dishRequestChangueStatus);
        oldDish.setStatus(newDish.getStatus());
        dishServicePort.changeStatus(id, oldDish);
        return dishResponseMapper.toDishResponse(oldDish);
    }

    @Override
    public List<DishResponsePaginated> getDishsRestaurantCategory(Integer numberOfRecords, Long idRestaurant, Long idCategory) {
        return dishResponsePaginatedMapper.toDishResponsePaginated(dishServicePort.getDishsRestaurantCategory(numberOfRecords, idRestaurant, idCategory));
    }


}
