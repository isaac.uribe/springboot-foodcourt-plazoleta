package com.example.springbootfoodcourtplazoleta.application.handler;

import com.example.springbootfoodcourtplazoleta.application.dto.*;

import java.util.List;

public interface OrderHandler {
    void saveOrder(OrderRequest orderRequest);
    List<OrderResponse> getOrdersPaginatedStatus(Integer numberOfRecords, String status, Long idRestaurant);
    OrderResponse assignedAnOrder(Long id, OrderRequestAssigned orderRequestAssigned);

    void sendingSms(Long idCustomer, Long idEmployee);
    void toDelivery(Long idCustomer, Long idEmployee, Integer code);
    void cancelOrder(Long idOrder, String email);
    List<OrderTimeResponse> getTimesOrder();
    List<RankingEmployeeResponse> getRankingEmployeeOrder();
}
