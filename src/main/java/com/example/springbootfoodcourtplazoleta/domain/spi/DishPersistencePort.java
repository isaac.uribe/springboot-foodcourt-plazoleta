package com.example.springbootfoodcourtplazoleta.domain.spi;

import com.example.springbootfoodcourtplazoleta.domain.model.Dish;

import java.util.List;

public interface DishPersistencePort {
    void saveDish(Dish dish);
    List<Dish> getAllDish();
    Dish getDish(Long id);

    Dish updateDish(Long id, Dish dish);
    Dish changeStatus(Long id, Dish dish);
    List<Dish> getDishsRestaurantCategory(Integer numberOfRecords, Long idRestaurant, Long idCategory);
}
