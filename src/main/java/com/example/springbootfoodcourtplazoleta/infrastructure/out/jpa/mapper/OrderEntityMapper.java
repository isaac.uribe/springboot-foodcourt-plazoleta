package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.mapper;

import com.example.springbootfoodcourtplazoleta.domain.model.Order;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.OrderEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(
        componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface OrderEntityMapper {
    OrderEntity toOrderEntity(Order order);

    Order toOrder(OrderEntity orderEntity);
}
