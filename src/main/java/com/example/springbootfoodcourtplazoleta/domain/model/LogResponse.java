package com.example.springbootfoodcourtplazoleta.domain.model;

import java.time.LocalDateTime;

public class LogResponse {
    private LocalDateTime dateTime;
    private String previousStatus;
    private String newStatus;

    public LogResponse() {
    }

    public LogResponse(LocalDateTime dateTime, String previousStatus, String newStatus) {
        this.dateTime = dateTime;
        this.previousStatus = previousStatus;
        this.newStatus = newStatus;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getPreviousStatus() {
        return previousStatus;
    }

    public void setPreviousStatus(String previousStatus) {
        this.previousStatus = previousStatus;
    }

    public String getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(String newStatus) {
        this.newStatus = newStatus;
    }
}
