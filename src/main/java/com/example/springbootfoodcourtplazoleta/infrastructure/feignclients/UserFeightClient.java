package com.example.springbootfoodcourtplazoleta.infrastructure.feignclients;

import com.example.springbootfoodcourtplazoleta.domain.model.User;
import com.example.springbootfoodcourtplazoleta.domain.model.UserAuth;
import com.example.springbootfoodcourtplazoleta.domain.spi.UserPersistencePort;
import com.example.springbootfoodcourtplazoleta.infrastructure.configuration.ClientConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "foodcourtusers", url = "http://localhost:8080", configuration = ClientConfiguration.class)
public interface UserFeightClient extends UserPersistencePort {

    @GetMapping("/auth/getOne/{id}")
    User getUser(@PathVariable(value="id") Long id);

    @GetMapping("/auth/getByEmail/{email}")
    UserAuth getUserByEmail(@PathVariable(value = "email") String email);
}
