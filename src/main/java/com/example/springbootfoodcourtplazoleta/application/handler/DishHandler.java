package com.example.springbootfoodcourtplazoleta.application.handler;

import com.example.springbootfoodcourtplazoleta.application.dto.*;

import java.util.List;

public interface DishHandler {
    void saveDish(DishRequest dishRequest);

    List<DishResponse> getAllDish();
    DishResponse getDish(Long id);

    DishResponse updateDish(Long id, DishRequestForUpdate dishRequestForUpdate);

    DishResponse changueStatus(Long id, DishRequestChangueStatus dishRequestChangueStatus);
    List<DishResponsePaginated> getDishsRestaurantCategory(Integer numberOfRecords, Long idRestaurant, Long idCategory);
}
