package com.example.springbootfoodcourtplazoleta.infrastructure.exception;

public class IdNoPresent extends RuntimeException{
    public IdNoPresent() {
        super();
    }
}
