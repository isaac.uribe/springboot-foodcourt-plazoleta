package com.example.springbootfoodcourtplazoleta.domain.exceptionhandler;

import com.example.springbootfoodcourtplazoleta.domain.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Collections;
import java.util.Map;

@ControllerAdvice
public class ControllerAdvisor {
    private static final String MESSAGE = "Message";

    @ExceptionHandler(IncorrectRol.class)
    public ResponseEntity<Map<String, String>> incorrectRol(IncorrectRol incorrectRol){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.INCORRECT_ROL.getMessage()));
    }

    @ExceptionHandler(FielNoNumeric.class)
    public ResponseEntity<Map<String, String>> fieldNoNumeric(FielNoNumeric fielNoNumeric){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.FIEL_ONLY_NUMERIC.getMessage()));
    }
    @ExceptionHandler(FielContainOnlyNumeric.class)
    public ResponseEntity<Map<String, String>> fielContainOnlyNumeric(FielContainOnlyNumeric fielContainOnlyNumeric){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.FIEL_CONTAIN_ONLY_NUMERIC.getMessage()));
    }

    @ExceptionHandler(EmptyField.class)
    public ResponseEntity<Map<String, String>> emptyField(EmptyField emptyField){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.EMPTY_FIELD.getMessage()));
    }

    @ExceptionHandler(IdNotExist.class)
    public ResponseEntity<Map<String,String>> idNotExist(IdNotExist idNotExist){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.ID_NOT_EXIST.getMessage()));
    }
    @ExceptionHandler(NegativePrice.class)
    public ResponseEntity<Map<String, String>> negativePrice(NegativePrice negativePrice){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.NEGATIVE_PRICE.getMessage()));
    }
    @ExceptionHandler(NoPermissionForUpdate.class)
    public ResponseEntity<Map<String,String>> noPermissionForUpdate(NoPermissionForUpdate noPermissionForUpdate){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.NO_PERMISSION_FOR_UPDATE.getMessage()));
    }
    @ExceptionHandler(ListEmpty.class)
    public ResponseEntity<Map<String, String>> listEmpty(ListEmpty listEmpty){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.LIST_DISH_EMPTY.getMessage()));
    }
    @ExceptionHandler(EmailNotExist.class)
    public ResponseEntity<Map<String, String>> emailNotExist(EmailNotExist emailNotExist){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.EMAIL_NOT_EXIST.getMessage()));
    }
}
