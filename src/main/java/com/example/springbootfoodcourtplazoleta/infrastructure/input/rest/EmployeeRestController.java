package com.example.springbootfoodcourtplazoleta.infrastructure.input.rest;

import com.example.springbootfoodcourtplazoleta.application.dto.OrderRequestAssigned;
import com.example.springbootfoodcourtplazoleta.application.dto.OrderResponse;
import com.example.springbootfoodcourtplazoleta.application.handler.OrderHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
@RequiredArgsConstructor
public class EmployeeRestController {
    private final OrderHandler orderHandler;

    @Operation(summary = "Get All Orders Paginated")
    @ApiResponse(responseCode = "200", description = "All Orders found")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @GetMapping("/getOrders")
    public ResponseEntity<List<OrderResponse>> getOrdersPaginatedStatus(
            @RequestParam Integer numberOfRecords,
            @RequestParam String status,
            @RequestParam Long idRestaurant
    ){
        return ResponseEntity.ok(orderHandler.getOrdersPaginatedStatus(numberOfRecords, status, idRestaurant));
    }
    @Operation(summary = "Updated Order to IN_PREPARATION and Assigned a chef")
    @ApiResponse(responseCode = "200", description = "Assigned Chef")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @PutMapping("/assignedOrder/{id}")
    public ResponseEntity<OrderResponse> assignedAnOrder(@PathVariable(value = "id")Long id,
                                                         @RequestBody OrderRequestAssigned orderRequestAssigned){
        return ResponseEntity.ok(orderHandler.assignedAnOrder(id, orderRequestAssigned));
    }

    @Operation(summary = "Updated Order to READY and SMS id sent to Customer")
    @ApiResponse(responseCode = "200", description = "SMS sent")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @PutMapping("/sendingSms")
    public ResponseEntity<Void> sendingMessage(@RequestParam Long idCustomer, @RequestParam Long idEmployee){
        orderHandler.sendingSms(idCustomer, idEmployee);
        return ResponseEntity.ok().build();
    }
    @Operation(summary = "Update Order to Delivery")
    @ApiResponse(responseCode = "200", description = "Updated to Delivery")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @PutMapping("/updateDelivery")
    public ResponseEntity<Void> updateDelivery(
            @RequestParam Long idCustomer,
            @RequestParam Long idEmployee,
            @RequestParam Integer code
            ){
        orderHandler.toDelivery(idCustomer, idEmployee, code);
        return ResponseEntity.ok().build();
    }
}
