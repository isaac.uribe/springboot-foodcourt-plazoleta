package com.example.springbootfoodcourtplazoleta.application.mapper;

import com.example.springbootfoodcourtplazoleta.application.dto.OrderDetailResponse;
import com.example.springbootfoodcourtplazoleta.application.dto.OrderResponse;
import com.example.springbootfoodcourtplazoleta.domain.model.Order;
import com.example.springbootfoodcourtplazoleta.domain.model.OrderDetail;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface OrderResponseMapper {

    @Mapping(target = "details", qualifiedByName = "toListOrderDetails")
    public OrderResponse toOrderResponse(Order order);
    public List<OrderResponse> toOrderResponseList(List<Order> orders);

    @Named("toListOrderDetails")
    default List<OrderDetailResponse> toListOrderDetails(List<OrderDetail> orderDetails){
        return orderDetails.stream()
                .map(this::toOrderDetailsResponse)
                .collect(Collectors.toList());
    }

    @Mapping(target = "name", source = "dish.name")
    OrderDetailResponse toOrderDetailsResponse(OrderDetail orderDetail);
}
