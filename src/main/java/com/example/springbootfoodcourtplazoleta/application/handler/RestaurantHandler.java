package com.example.springbootfoodcourtplazoleta.application.handler;

import com.example.springbootfoodcourtplazoleta.application.dto.RestaurantRequest;
import com.example.springbootfoodcourtplazoleta.application.dto.RestaurantResponse;
import com.example.springbootfoodcourtplazoleta.application.dto.RestaurantResponsePaginated;
import org.springframework.data.domain.Page;

import java.util.List;

public interface RestaurantHandler {

    void saveRestaurant(RestaurantRequest restaurantRequest);

    List<RestaurantResponse> getAllRestaurants();
    RestaurantResponse getRestaurant(Long id);

    List<RestaurantResponsePaginated> getAllRestaurantPaginated(Integer numberOfRecords);
}
