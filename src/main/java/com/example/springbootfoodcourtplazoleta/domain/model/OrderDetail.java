package com.example.springbootfoodcourtplazoleta.domain.model;

public class OrderDetail {
    private Long id;
    private Dish dish;
    private Integer quantity;

    public OrderDetail() {
    }

    public OrderDetail(Long id, Dish dish, Integer quantity) {
        this.id = id;
        this.dish = dish;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
