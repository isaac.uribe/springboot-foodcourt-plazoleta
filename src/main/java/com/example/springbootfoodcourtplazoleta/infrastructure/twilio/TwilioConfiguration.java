package com.example.springbootfoodcourtplazoleta.infrastructure.twilio;

import com.twilio.Twilio;
import com.twilio.rest.verify.v2.service.Verification;
import lombok.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
@Configuration
//@ConfigurationProperties("twilio")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TwilioConfiguration {
    @Value("${twilio.account-sid}")
    private String accountSid;
    @Value("${twilio.auth-token}")
    private String authToken;
    @Value("${twilio.trial-number}")
    private String trialNumber;

}
