package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.adapter;

import com.example.springbootfoodcourtplazoleta.domain.exception.EmailNotExist;
import com.example.springbootfoodcourtplazoleta.domain.model.*;
import com.example.springbootfoodcourtplazoleta.domain.spi.OrderPersistencePort;
import com.example.springbootfoodcourtplazoleta.infrastructure.exception.*;
import com.example.springbootfoodcourtplazoleta.infrastructure.feignclients.LogFeightClient;
import com.example.springbootfoodcourtplazoleta.infrastructure.feignclients.UserFeightClient;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.OrderEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.RestaurantEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.StatusOfOrder;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.mapper.OrderEntityMapper;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository.OrderRepository;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository.RestaurantRepository;
import com.example.springbootfoodcourtplazoleta.infrastructure.twilio.TwilioConfiguration;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.jar.JarOutputStream;

@RequiredArgsConstructor
public class OrderJpaAdapter implements OrderPersistencePort {
    private final OrderRepository orderRepository;
    private final OrderEntityMapper orderEntityMapper;
    private final RestaurantRepository restaurantRepository;
    private final UserFeightClient userFeightClient;
    private final TwilioConfiguration twilioConfiguration;
    private final LogFeightClient logFeightClient;
    Random random = new Random();
    private Map<String, Integer> codeToPhoneMap = new HashMap<>();
    User user = new User();

    UserAuth userAuth = new UserAuth();
    @Override
    public void saveOrder(Order order) {
        List<StatusOfOrder> statusOfOrderList = Arrays.asList(StatusOfOrder.PENDING, StatusOfOrder.IN_PREPARATION, StatusOfOrder.READY);
        List<OrderEntity> orderEntityList = orderRepository.findByIdCustomerAndStatusIn(order.getIdCustomer(), statusOfOrderList);
        if (!orderEntityList.isEmpty()){
            throw new OrderInProcess();
        }
        orderRepository.save(orderEntityMapper.toOrderEntity(order));
    }

    @Override
    public List<Order> getOrdersPaginatedStatus(Integer numberOfRecords, String status, Long idRestaurant) {
        try {
            Pageable pagerList = PageRequest.of(0, numberOfRecords);
            Optional<RestaurantEntity> restaurantEntity = restaurantRepository.findById(idRestaurant);
            StatusOfOrder statusOfOrder = getStatusFromString(status);
            Page<OrderEntity> orderEntityList = orderRepository.findByStatusAndRestaurant(statusOfOrder, restaurantEntity.get(), pagerList);
            Page<Order> orders = orderEntityList.map(orderEntity -> orderEntityMapper.toOrder(orderEntity));

            return orders.getContent();
        }catch (Exception e){
            System.out.println(e.getMessage());
            throw new PaginatonNotNull();
        }

    }

    @Override
    public Order assignedAnOrder(Long id, Order order) {
        Optional<OrderEntity> orderEntity = orderRepository.findById(id);
        if (!orderEntity.isPresent()){
            throw new IdNoPresent();
        }
        OrderEntity currentOrder = orderEntity.get();
        if (currentOrder.getIdChef() != null){
            throw new OrderHasIdChef();
        }

        OrderEntity orderEntity1 = orderEntityMapper.toOrderEntity(order);
        User userCustomer = userFeightClient.getUser(currentOrder.getIdCustomer());
        User userEmployee = userFeightClient.getUser(order.getIdChef());

        LogRequest logRequest = new LogRequest();
        logRequest.setIdOrder(currentOrder.getId());
        logRequest.setIdCustomer(currentOrder.getIdCustomer());
        logRequest.setEmailCustomer(userCustomer.getEmail());
        logRequest.setPreviousStatus(currentOrder.getStatus().name());
        logRequest.setNewStatus(order.getStatus());
        logRequest.setIdEmployee(order.getIdChef());
        logRequest.setEmailEmployee(userEmployee.getEmail());
        logFeightClient.saveLog(logRequest);

        orderRepository.save(orderEntity1);

        return order;

    }
    @Override
    public Order getOrder(Long id) {
        Optional<OrderEntity> orderEntity = orderRepository.findById(id);
        if (!orderEntity.isPresent()){
            throw new IdNoPresent();
        }
        OrderEntity order = orderEntity.get();
        return orderEntityMapper.toOrder(order);
    }

    @Override
    public void sendingSms(Long idCustomer, Long idEmployee) {

        Integer codeNumber = random.nextInt(10000);

        String message = "Hi, your order is ready, this is your code to claim " + codeNumber;
        try {
            user = userFeightClient.getUser(idCustomer);
        }catch (Exception e){
            throw new IdNoPresent();
        }
        Optional<OrderEntity> orderEntityOptional = orderRepository.findByStatusAndIdCustomerAndIdChef(StatusOfOrder.IN_PREPARATION, idCustomer, idEmployee);

        if (!orderEntityOptional.isPresent()){
            throw new OrderInPreparationNotExist();
        }

        OrderEntity order = orderEntityOptional.get();
        User userEmployee = userFeightClient.getUser(idEmployee);

        LogRequest logRequest = new LogRequest();
        logRequest.setIdOrder(order.getId());
        logRequest.setIdCustomer(order.getIdCustomer());
        logRequest.setEmailCustomer(user.getEmail());
        logRequest.setPreviousStatus(order.getStatus().name());
        logRequest.setIdEmployee(order.getIdChef());
        logRequest.setEmailEmployee(userEmployee.getEmail());

        order.setStatus(StatusOfOrder.READY);
        logRequest.setNewStatus(order.getStatus().name());
        logFeightClient.saveLog(logRequest);

        orderRepository.save(order);

        codeToPhoneMap.put(user.getCellPhone(), codeNumber);

        PhoneNumber to = new PhoneNumber(user.getCellPhone());
        PhoneNumber from = new PhoneNumber(twilioConfiguration.getTrialNumber());
        MessageCreator creator = Message.creator(to, from, message);
        creator.create();
    }
    @Override
    public void toDelivery(Long idCustomer, Long idEmployee, Integer code) {
        try {
            user = userFeightClient.getUser(idCustomer);
        }catch (Exception e){
            throw new IdNoPresent();
        }
        Integer currentCode = codeToPhoneMap.get(user.getCellPhone());
        Optional<OrderEntity> orderEntityOptional = orderRepository.findByStatusAndIdCustomerAndIdChef(StatusOfOrder.READY, idCustomer, idEmployee);
        if (!orderEntityOptional.isPresent()){
            throw new OrderReadyNotExist();
        }
        if (currentCode == null || !currentCode.equals(code)){
            throw new NotEqual();
        }
        codeToPhoneMap.remove(user.getCellPhone());
        OrderEntity orderEntity = orderEntityOptional.get();
        User userEmployee = userFeightClient.getUser(idEmployee);

        LogRequest logRequest = new LogRequest();
        logRequest.setIdOrder(orderEntity.getId());
        logRequest.setIdCustomer(orderEntity.getIdCustomer());
        logRequest.setEmailCustomer(user.getEmail());
        logRequest.setPreviousStatus(orderEntity.getStatus().name());
        logRequest.setIdEmployee(orderEntity.getIdChef());
        logRequest.setEmailEmployee(userEmployee.getEmail());


        orderEntity.setStatus(StatusOfOrder.DELIVERED);
        orderEntity.setDateEnd(LocalDateTime.now());
        Duration duration = Duration.between(orderEntity.getDate(), orderEntity.getDateEnd());
        orderEntity.setTimeOrder(duration);
        logRequest.setNewStatus(orderEntity.getStatus().name());
        logFeightClient.saveLog(logRequest);
        orderRepository.save(orderEntity);
    }

    @Override
    public void cancelOrder(Long idOrder, String email) {
        try {
            userAuth = userFeightClient.getUserByEmail(email);
        }catch (Exception e){
            throw new EmailNotExist();
        }

        Optional<OrderEntity> orderEntity = orderRepository.findById(idOrder);
        if (!orderEntity.isPresent()){
            throw new IdNoPresent();
        }
        OrderEntity order = orderEntity.get();
        if (!order.getIdCustomer().equals(userAuth.getId())){
            throw new OrderCustomerWrong();
        }
        if (!order.getStatus().equals(StatusOfOrder.PENDING)){
            throw new OrderInPreparation();
        }
        order.setStatus(StatusOfOrder.CANCELLED);
        orderRepository.save(order);

    }
    @Override
    public List<OrderTime> getTimesOrder() {
        List<OrderEntity> orderEntities = orderRepository.findByStatus(StatusOfOrder.DELIVERED);
        List<OrderTime> orderTimes = new ArrayList<>();
        for (OrderEntity orderEntity: orderEntities){
            LocalDateTime timeStar = orderEntity.getDate();
            LocalDateTime timeEnd = orderEntity.getDateEnd();

            Duration duration = Duration.between(timeStar, timeEnd);
            Long hour = duration.toHours() % 24;
            Long minutes = duration.toMinutes() % 60;
            Long seconds = duration.getSeconds() % 60;
            OrderTime orderTime = new OrderTime(orderEntity.getId(), orderEntity.getStatus().name(),
                    orderEntity.getIdChef(),orderEntity.getDate(),orderEntity.getDateEnd(), hour, minutes, seconds);
            orderTimes.add(orderTime);
        }
        return orderTimes;
    }

    @Override
    public List<RankingEmployee> getRankingEmployeeOrder() {
        List<RankingEmployee> ranking = new ArrayList<>();
        List<OrderEntity> orderEntities = orderRepository.findAll();
        Set<Long> processedChef = new HashSet<>();
        for (OrderEntity orderEntity : orderEntities){

            if (processedChef.contains(orderEntity.getIdChef())){
                continue;
            }
            Long averageTime = (long) calculateTimeEmployee(orderEntity);
            User user1 = userFeightClient.getUser(orderEntity.getIdChef());
            ranking.add(new RankingEmployee(orderEntity.getIdChef(), averageTime, user1.getName()));
            processedChef.add(orderEntity.getIdChef());
        }
        ranking.sort(Comparator.comparing(RankingEmployee::getTotalTime));
        return ranking;
    }
    public double calculateTimeEmployee(OrderEntity order){
        List<OrderEntity> orderEntities = orderRepository.findByIdChef(order.getIdChef());

        Long totalTime = 0L;

        for (OrderEntity orderEntity : orderEntities){
            LocalDateTime timeStar = orderEntity.getDate();
            LocalDateTime timeEnd = orderEntity.getDateEnd();
            Long timeOrder = Duration.between(timeStar, timeEnd).toSeconds();
            totalTime += timeOrder;
        }
        return totalTime / (double) orderEntities.size();
    }


    public StatusOfOrder getStatusFromString(String status){
        for (StatusOfOrder value : StatusOfOrder.values()){
            if (value.name().equalsIgnoreCase(status)){
                return value;
            }
        }
        throw new StatusNotExist();
    }
}
