package com.example.springbootfoodcourtplazoleta.domain.validation;

import com.example.springbootfoodcourtplazoleta.domain.exception.EmptyField;
import com.example.springbootfoodcourtplazoleta.domain.exception.NegativePrice;
import com.example.springbootfoodcourtplazoleta.domain.model.Category;
import com.example.springbootfoodcourtplazoleta.domain.model.Restaurant;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DishValidationTest {

    DishValidation dishValidation = new DishValidation();
    @Test
    void validateEmptyField() {
        String data = "ejemplo";
        String dataNull = null;
        assertDoesNotThrow(()->dishValidation.validateEmptyField(data));
        assertThrows(EmptyField.class, ()->dishValidation.validateEmptyField(dataNull));
    }

    @Test
    void validatePriceDish() {
        Integer price = 100;
        Integer priceNegative = -100;
        assertDoesNotThrow(()->dishValidation.validatePriceDish(price));
        assertThrows(NegativePrice.class, ()->dishValidation.validatePriceDish(priceNegative));
    }

    @Test
    void validateRestaurant() {
        Restaurant restaurant = new Restaurant();
        Restaurant restaurantNull = null;
        assertDoesNotThrow(()->dishValidation.validateRestaurant(restaurant));
        assertThrows(EmptyField.class, ()->dishValidation.validateRestaurant(restaurantNull));
    }

    @Test
    void validateCategory() {
        Category category = new Category();
        Category categoryNull = null;
        assertDoesNotThrow(()->dishValidation.validateCategory(category));
        assertThrows(EmptyField.class, ()->dishValidation.validateCategory(categoryNull));
    }
    @Test
    void validateStatus(){
        Boolean status = true;
        Boolean statusNull = null;
        assertDoesNotThrow(()->dishValidation.validateStatus(status));
        assertThrows(EmptyField.class, ()->dishValidation.validateStatus(statusNull));
    }
}