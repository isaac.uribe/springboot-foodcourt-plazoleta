package com.example.springbootfoodcourtplazoleta.domain.api;

import com.example.springbootfoodcourtplazoleta.domain.model.LogResponse;

import java.util.List;

public interface LogServicePort {
    List<LogResponse> getLogs(Long idCustomer, Long idOrder);
}
