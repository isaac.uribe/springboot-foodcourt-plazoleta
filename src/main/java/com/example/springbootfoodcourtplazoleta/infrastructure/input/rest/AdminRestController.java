package com.example.springbootfoodcourtplazoleta.infrastructure.input.rest;

import com.example.springbootfoodcourtplazoleta.application.dto.RestaurantRequest;
import com.example.springbootfoodcourtplazoleta.application.dto.RestaurantResponse;
import com.example.springbootfoodcourtplazoleta.application.dto.RestaurantResponsePaginated;
import com.example.springbootfoodcourtplazoleta.application.handler.RestaurantHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin")
@RequiredArgsConstructor
public class AdminRestController {
    private final RestaurantHandler restaurantHandler;

    @Operation(summary = "Create a Restaurant")
    @ApiResponse(responseCode = "201", description = "Restaurant created successfully")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @PostMapping("/createRestaurant")
    public ResponseEntity<Void> saveRestaurant(@RequestBody RestaurantRequest restaurantRequest){
        restaurantHandler.saveRestaurant(restaurantRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Operation(summary = "Get All Restaurants form the database")
    @ApiResponse(responseCode = "200", description = "All Restaurants found")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @GetMapping("/findAllRestaurant")
    public ResponseEntity<List<RestaurantResponse>> getAllRestaurants(){
        return ResponseEntity.ok(restaurantHandler.getAllRestaurants());
    }

    @Operation(summary = "Get One Restaurant from the database")
    @ApiResponse(responseCode = "200", description = "Restaurant found")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @GetMapping("findRestaurant/{id}")
    public ResponseEntity<RestaurantResponse> getRestaurant(@PathVariable Long id){
        return ResponseEntity.ok(restaurantHandler.getRestaurant(id));
    }

//    @GetMapping("/getRestaurantPaginated")
//    public ResponseEntity<List<RestaurantResponsePaginated>> getRestaurantsPaginatedU(@RequestParam Integer numberOfRecords){
//        return ResponseEntity.ok(restaurantHandler.getAllRestaurantPaginated(numberOfRecords));
//    }
}
