package com.example.springbootfoodcourtplazoleta.infrastructure.exceptionhandler;

public enum ExceptionResponse {
    ID_NO_PRESENT("El id ingresado no existe"),
    USER_NOT_FOUND("usuario no encontrado"),
    PAGINATION_NOT_NULL("Ha ocurrido un error al momento de ingresar los parametros por favor revise que sean correctos"),
    ORDER_IN_PROCESS("No puedes crear una nueva orden por que ya tienes una anterior en proceso"),
    STATUS_NOT_EXIST("El estado por el que esta buscando la orden no existe"),
    ORDER_IN_PREPARATION_NOT_EXIST("No existe una orden que tanga los ids ingresados y que se encuentre en preparacion"),
    ORDER_READY_NOT_EXIST("No existe una order que tenga los ids ingresados y que se encuentre lista"),
    NOT_EQUAL("El codigo ingresado no es correcto"),
    ORDER_CUSTOMER_WRONG("La orden que estas intentando cancelar no te pertenece o hay un error con el correo ingresado"),
    ORDER_IN_PREPARATION("Lo sentimos, tu pedido ya esta en preparacion y no puede cancelarse"),
    ORDER_HAS_ID_CHEF("La orden ya tiene un id asignado");

    private String message;

    ExceptionResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
