package com.example.springbootfoodcourtplazoleta.domain.usecase;

import com.example.springbootfoodcourtplazoleta.domain.api.DishServicePort;
import com.example.springbootfoodcourtplazoleta.domain.exception.NoPermissionForUpdate;
import com.example.springbootfoodcourtplazoleta.domain.model.Dish;
import com.example.springbootfoodcourtplazoleta.domain.model.User;
import com.example.springbootfoodcourtplazoleta.domain.spi.DishPersistencePort;
import com.example.springbootfoodcourtplazoleta.domain.spi.UserPersistencePort;
import com.example.springbootfoodcourtplazoleta.domain.validation.DishValidation;
import jakarta.servlet.http.HttpSession;

import java.util.List;

public class DishUseCase implements DishServicePort {
    private final DishPersistencePort dishPersistencePort;
    private final DishValidation dishValidation;
    private final UserPersistencePort userPersistencePort;
    private final HttpSession session;

    public DishUseCase(DishPersistencePort dishPersistencePort, DishValidation dishValidation, UserPersistencePort userPersistencePort, HttpSession session) {
        this.dishPersistencePort = dishPersistencePort;
        this.dishValidation = dishValidation;
        this.userPersistencePort = userPersistencePort;
        this.session = session;
    }

    @Override
    public void saveDish(Dish dish) {
        dishValidation.validateEmptyField(dish.getName());
        dishValidation.validatePriceDish(dish.getPrice());
        dishValidation.validateEmptyField(dish.getDescription());
        dishValidation.validateEmptyField(dish.getUrlImg());
        dishValidation.validateCategory(dish.getCategory());
        dishValidation.validateRestaurant(dish.getRestaurant());
        dishPersistencePort.saveDish(dish);
    }

    @Override
    public List<Dish> getAllDish() {
        return dishPersistencePort.getAllDish();
    }

    @Override
    public Dish getDish(Long id) {
        return dishPersistencePort.getDish(id);
    }

    @Override
    public Dish updateDish(Long id, Dish dish) {
        dishValidation.validatePriceDish(dish.getPrice());
        dishValidation.validateEmptyField(dish.getDescription());
        User user = userPersistencePort.getUser(dish.getRestaurant().getIdUser());
        String userEmail = (String) session.getAttribute("userEmail");
        if (!user.getEmail().equals(userEmail)){
            throw new NoPermissionForUpdate();
        }
        return dishPersistencePort.updateDish(id, dish);
    }

    @Override
    public Dish changeStatus(Long id, Dish dish) {
        dishValidation.validateStatus(dish.getStatus());
        User user = userPersistencePort.getUser(dish.getRestaurant().getIdUser());
        String userEmail = (String) session.getAttribute("userEmail");
        if (!user.getEmail().equals(userEmail)){
            throw new NoPermissionForUpdate();
        }
        return  dishPersistencePort.changeStatus(id, dish);
    }

    @Override
    public List<Dish> getDishsRestaurantCategory(Integer numberOfRecords, Long idRestaurant, Long idCategory) {
        return dishPersistencePort.getDishsRestaurantCategory(numberOfRecords, idRestaurant, idCategory);
    }


}
