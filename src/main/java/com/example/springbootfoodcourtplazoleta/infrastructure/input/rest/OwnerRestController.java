package com.example.springbootfoodcourtplazoleta.infrastructure.input.rest;

import com.example.springbootfoodcourtplazoleta.application.dto.*;
import com.example.springbootfoodcourtplazoleta.application.handler.DishHandler;
import com.example.springbootfoodcourtplazoleta.application.handler.OrderHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/owner")
@RequiredArgsConstructor
public class OwnerRestController {
    private final DishHandler dishHandler;
    private final OrderHandler orderHandler;

    @Operation(summary = "Create a Dish")
    @ApiResponse(responseCode = "201", description = "Dish created successfully")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @PostMapping("/createDish")
    public ResponseEntity<Void> saveDish(@RequestBody DishRequest dishRequest){
        dishHandler.saveDish(dishRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    @Operation(summary = "Get All Dishes form the database")
    @ApiResponse(responseCode = "200", description = "All Dishes found")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @GetMapping("/getAllDishes")
    public ResponseEntity<List<DishResponse>> getAllDish(){
        return ResponseEntity.ok(dishHandler.getAllDish());
    }

    @Operation(summary = "Get One Dish from the database")
    @ApiResponse(responseCode = "200", description = "Dish found")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @GetMapping("/getDish/{id}")
    public ResponseEntity<DishResponse> getDish(@PathVariable(value = "id") Long id){
        return ResponseEntity.ok(dishHandler.getDish(id));
    }
    @Operation(summary = "Update Info Dish")
    @ApiResponse(responseCode = "200", description = "updated Dish")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @PutMapping("/updateDish/{id}")
    public ResponseEntity<DishResponse> updateDish(@PathVariable(value = "id")Long id, @RequestBody DishRequestForUpdate dishRequestForUpdate){
        return ResponseEntity.ok(dishHandler.updateDish(id,dishRequestForUpdate));
    }
    @Operation(summary = "Change the Status of Dish On or Off")
    @ApiResponse(responseCode = "200", description = "Status Changed")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @PutMapping("/changueStatusDish/{id}")
    public ResponseEntity<DishResponse> changueStatus(@PathVariable(value = "id")Long id, @RequestBody DishRequestChangueStatus dishRequestChangueStatus){
        return ResponseEntity.ok(dishHandler.changueStatus(id, dishRequestChangueStatus));
    }
    @Operation(summary = "Get Times Of the Orders")
    @ApiResponse(responseCode = "200", description = "Get Times of the orders")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @GetMapping("/getTimesOrders")
    public ResponseEntity<List<OrderTimeResponse>> getTimesOrder(){
        return ResponseEntity.ok(orderHandler.getTimesOrder());
    }

    @Operation(summary = "Get A Ranking of the Employees")
    @ApiResponse(responseCode = "200", description = "Get Rankign of the employees")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @GetMapping("/getRanking")
    public ResponseEntity<List<RankingEmployeeResponse>> getRanking(){
        return ResponseEntity.ok(orderHandler.getRankingEmployeeOrder());
    }
}
