package com.example.springbootfoodcourtplazoleta.domain.validation;

import com.example.springbootfoodcourtplazoleta.domain.exception.EmptyField;
import com.example.springbootfoodcourtplazoleta.domain.exception.FielContainOnlyNumeric;
import com.example.springbootfoodcourtplazoleta.domain.exception.FielNoNumeric;
import com.example.springbootfoodcourtplazoleta.domain.exception.IncorrectRol;
import com.example.springbootfoodcourtplazoleta.domain.exceptionhandler.ExceptionResponse;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

class RestaurantValidationTest {

    RestaurantValidation restaurantValidation = new RestaurantValidation();

    @Test
    void validateRol() {
        String rol = "OWNER";
        assertDoesNotThrow(()->restaurantValidation.validateRol(rol));
        assertThrows(IncorrectRol.class, ()->restaurantValidation.validateRol("Employee"));
    }

    @Test
    void validateNit() {
        String nit = "123456";
        String nitWrong = "46455asd45";
        assertDoesNotThrow(()->restaurantValidation.validateNit(nit));
        assertThrows(FielNoNumeric.class, ()->restaurantValidation.validateNit(nitWrong));
    }

    @Test
    void validatePhone() {
        String phone = "+12345632";
        String phoneWithLetter = "+4656asdas";
        String phoneExceedsSize = "+1231231231231231232";
        assertDoesNotThrow(()->restaurantValidation.validatePhone(phone));
        assertThrows(FielNoNumeric.class, ()->restaurantValidation.validatePhone(phoneWithLetter));
        assertThrows(FielNoNumeric.class, ()->restaurantValidation.validatePhone(phoneExceedsSize));
    }

    @Test
    void validateName() {

        String name = "le restaurante45";
        String nameOnlyNumeric = "123123123123";
        String nameNull = null;
        assertDoesNotThrow(()->restaurantValidation.validateName(name));
        assertThrows(FielContainOnlyNumeric.class, ()->restaurantValidation.validateName(nameOnlyNumeric));
        assertThrows(FielContainOnlyNumeric.class, ()->restaurantValidation.validateName(nameNull));
    }

    @Test
    void validateEmptyField() {
        String data = "ejemplo";
        String dataNull = null;
        assertDoesNotThrow(()->restaurantValidation.validateEmptyField(data));
        assertThrows(EmptyField.class,()->restaurantValidation.validateEmptyField(dataNull));
    }

    @Test
    void validateId() {
        Long id = 1L;
        Long idNull = null;
        assertDoesNotThrow(()->restaurantValidation.validateId(id));
        assertThrows(EmptyField.class, ()->restaurantValidation.validateId(idNull));
    }
}