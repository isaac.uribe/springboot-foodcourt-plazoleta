package com.example.springbootfoodcourtplazoleta.application.mapper;

import com.example.springbootfoodcourtplazoleta.application.dto.DishRequest;
import com.example.springbootfoodcourtplazoleta.domain.model.Dish;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface DishRequestMapper {
    Dish toDish(DishRequest dishRequest);
}
