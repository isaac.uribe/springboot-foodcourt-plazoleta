package com.example.springbootfoodcourtplazoleta.domain.usecase;

import com.example.springbootfoodcourtplazoleta.domain.exception.EmailNotExist;
import com.example.springbootfoodcourtplazoleta.domain.exception.IdNotExist;
import com.example.springbootfoodcourtplazoleta.domain.model.*;
import com.example.springbootfoodcourtplazoleta.domain.spi.OrderPersistencePort;
import com.example.springbootfoodcourtplazoleta.domain.spi.UserPersistencePort;
import com.example.springbootfoodcourtplazoleta.domain.validation.OrderValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class OrderUseCaseTest {
    Order order;
    OrderDetail orderDetail;
    Restaurant restaurant;
    UserAuth userAuth;
    Rol rol;
    User user;
    @InjectMocks
    OrderUseCase orderUseCase;
    @Mock
    OrderPersistencePort orderPersistencePort;
    @Mock
    UserPersistencePort userPersistencePort;
    @Mock
    OrderValidation orderValidation;

    @BeforeEach
    void setUp() {
        order = new Order();
        restaurant = new Restaurant();
        restaurant.setId(1L);
        order.setRestaurant(restaurant);
        orderDetail = new OrderDetail();
        List<OrderDetail> orderDetails = new ArrayList<>();
        orderDetails.add(orderDetail);
        order.setDetails(orderDetails);
        order.setEmailCustomer("ejemplo@gmail.com");
        userAuth = new UserAuth();
        userAuth.setId(1L);
        rol = new Rol();
        userAuth.setRol(rol);
        user = new User();
        user.setRol(rol);

        order.setIdCustomer(userAuth.getId());
        order.setDate(LocalDateTime.now());
    }

    @Test
    void saveOrder() {
        rol.setRol(Role.CUSTOMER);
        when(userPersistencePort.getUserByEmail(order.getEmailCustomer())).thenReturn(userAuth);
        orderUseCase.saveOrder(order);
        verify(userPersistencePort, times(1)).getUserByEmail(order.getEmailCustomer());
    }
    @Test
    void saveOrderEmailNotExist(){
        assertThrows(EmailNotExist.class, ()->orderUseCase.saveOrder(order));
    }
    @Test
    void getOrdersPaginatedStatus(){
        List<Order> orderList = new ArrayList<>();
        orderList.add(order);
        String status = "PENDING";
        when(orderPersistencePort.getOrdersPaginatedStatus(1, status, 1L)).thenReturn(orderList);
        List<Order> orders = orderUseCase.getOrdersPaginatedStatus(1, status, 1L);
        verify(orderPersistencePort, times(1)).getOrdersPaginatedStatus(1, status, 1L);
    }
    @Test
    void assignedAnOrder(){
        rol.setRol(Role.EMPLOYEE);
        order.setIdChef(1L);
        when(userPersistencePort.getUser(1L)).thenReturn(user);
        orderUseCase.assignedAnOrder(1L, order);
        verify(userPersistencePort, times(1)).getUser(1L);
    }
    @Test
    void assignedAnOrderWithError(){
        assertThrows(IdNotExist.class, ()->orderUseCase.assignedAnOrder(1L, order));
    }
    @Test
    void getOrder(){
        when(orderPersistencePort.getOrder(1L)).thenReturn(order);
        Order result = orderUseCase.getOrder(1L);
        assertEquals(result, order);
    }
    @Test
    void sendingSms(){
        Long idCustomer = 1L;
        Long idEmployee = 2L;
        orderUseCase.sendingSms(idCustomer, idEmployee);
        verify(orderPersistencePort, times(1)).sendingSms(idCustomer, idEmployee);
    }
    @Test
    void toDelivery(){
        Long idCustomer = 1L;
        Long idEmployee = 2L;
        Integer code = 1234;
        orderUseCase.toDelivery(idCustomer, idEmployee, code);
        verify(orderPersistencePort, times(1)).toDelivery(idCustomer, idEmployee, code);
    }
    @Test
    void cancelOrder(){
        orderUseCase.cancelOrder(1L, "ejemplo@gmail.com");
        verify(orderPersistencePort, times(1)).cancelOrder(1L, "ejemplo@gmail.com");
    }
    @Test
    void getTimesOrder(){
        List<OrderTime> getTimesOrder = orderUseCase.getTimesOrder();
        verify(orderPersistencePort, times(1)).getTimesOrder();

    }
    @Test
    void getRankingEmployeeOrder(){
        List<RankingEmployee> rankingEmployees = orderUseCase.getRankingEmployeeOrder();
        verify(orderPersistencePort, times(1)).getRankingEmployeeOrder();
    }
}