package com.example.springbootfoodcourtplazoleta.application.dto;

import com.example.springbootfoodcourtplazoleta.domain.model.Category;
import com.example.springbootfoodcourtplazoleta.domain.model.Restaurant;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DishRequest {
    private String name;
    private Category category;
    private String description;
    private Integer price;
    private Restaurant restaurant;
    private String urlImg;
}
