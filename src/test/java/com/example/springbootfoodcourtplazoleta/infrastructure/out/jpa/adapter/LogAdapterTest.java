package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.adapter;

import com.example.springbootfoodcourtplazoleta.domain.model.LogResponse;
import com.example.springbootfoodcourtplazoleta.infrastructure.feignclients.LogFeightClient;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class LogAdapterTest {
    @InjectMocks
    LogAdapter logAdapter;
    @Mock
    LogFeightClient logFeightClient;
    @Test
    void getLogs() {
        List<LogResponse> logResponses = new ArrayList<>();
        logResponses.add(new LogResponse());
        Mockito.when(logFeightClient.getLogs(1L,1L)).thenReturn(logResponses);
        List<LogResponse> result = logAdapter.getLogs(1L,1L);
        assertEquals(result, logResponses);
    }
}