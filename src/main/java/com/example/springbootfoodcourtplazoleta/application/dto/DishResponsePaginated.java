package com.example.springbootfoodcourtplazoleta.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DishResponsePaginated {
    private String name;
    private String description;
    private Integer price;
    private String urlImg;
}
