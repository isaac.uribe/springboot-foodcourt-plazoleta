package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository;

import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRespository extends JpaRepository<CategoryEntity, Long> {
}
