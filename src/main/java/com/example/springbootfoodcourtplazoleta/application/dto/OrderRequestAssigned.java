package com.example.springbootfoodcourtplazoleta.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderRequestAssigned {
    private Long idChef;
}
