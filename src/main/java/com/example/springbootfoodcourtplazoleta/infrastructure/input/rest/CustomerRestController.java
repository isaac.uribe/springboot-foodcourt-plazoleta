package com.example.springbootfoodcourtplazoleta.infrastructure.input.rest;

import com.example.springbootfoodcourtplazoleta.application.dto.DishResponsePaginated;
import com.example.springbootfoodcourtplazoleta.application.dto.OrderRequest;
import com.example.springbootfoodcourtplazoleta.application.dto.RestaurantResponsePaginated;
import com.example.springbootfoodcourtplazoleta.application.handler.DishHandler;
import com.example.springbootfoodcourtplazoleta.application.handler.LogHandler;
import com.example.springbootfoodcourtplazoleta.application.handler.OrderHandler;
import com.example.springbootfoodcourtplazoleta.application.handler.RestaurantHandler;
import com.example.springbootfoodcourtplazoleta.domain.model.Dish;
import com.example.springbootfoodcourtplazoleta.domain.model.LogResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
public class CustomerRestController {

    private final RestaurantHandler restaurantHandler;
    private final DishHandler dishHandler;
    private final OrderHandler orderHandler;
    private final LogHandler logHandler;
    @Operation(summary = "Get All Restaurants Paginated")
    @ApiResponse(responseCode = "200", description = "All Restaurants found")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @GetMapping("/getRestaurantPaginated")
    public ResponseEntity<List<RestaurantResponsePaginated>> getRestaurantsPaginated(@RequestParam Integer numberOfRecords){
        return ResponseEntity.ok(restaurantHandler.getAllRestaurantPaginated(numberOfRecords));
    }
    @Operation(summary = "Get Dishes Paginated for Restaurant and Category")
    @ApiResponse(responseCode = "200", description = "Dishes found")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @GetMapping("/getDishRestaurantCategory")
    public ResponseEntity<List<DishResponsePaginated>> getDishRestaurantCategory(
            @RequestParam Integer numberOfRecords,
            @RequestParam Long idRestaurant,
            @RequestParam Long idCategory
    ){
        return ResponseEntity.ok(dishHandler.getDishsRestaurantCategory(numberOfRecords, idRestaurant, idCategory));
    }
    @Operation(summary = "Create a Order")
    @ApiResponse(responseCode = "201", description = "Order created successfully")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @PostMapping("/createOrder")
    public ResponseEntity<Void> createOrder(@RequestBody OrderRequest orderRequest){
        orderHandler.saveOrder(orderRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Operation(summary = "Cancel a Order")
    @ApiResponse(responseCode = "200", description = "Order Cancelled")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @PutMapping("/cancelOrder")
    public ResponseEntity<Void> calcelOrder(@RequestParam Long idOrder, @RequestParam String email){
        orderHandler.cancelOrder(idOrder, email);
        return ResponseEntity.ok().build();
    }
    @Operation(summary = "Get logs")
    @ApiResponse(responseCode = "200", description = "Get Logs of the order")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @GetMapping("/getLogs")
    public ResponseEntity<List<LogResponse>> getLogs(@RequestParam Long idCustomer, @RequestParam Long idOrder){
        return ResponseEntity.status(HttpStatus.OK).body(logHandler.getLogs(idCustomer, idOrder));
    }
}
