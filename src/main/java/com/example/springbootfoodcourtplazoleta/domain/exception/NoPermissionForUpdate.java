package com.example.springbootfoodcourtplazoleta.domain.exception;

public class NoPermissionForUpdate extends RuntimeException{
    public NoPermissionForUpdate() {
        super();
    }
}
