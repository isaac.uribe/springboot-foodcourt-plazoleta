package com.example.springbootfoodcourtplazoleta.domain.spi;

import com.example.springbootfoodcourtplazoleta.domain.model.Order;
import com.example.springbootfoodcourtplazoleta.domain.model.OrderTime;
import com.example.springbootfoodcourtplazoleta.domain.model.RankingEmployee;

import java.util.List;

public interface OrderPersistencePort {
    void saveOrder(Order order);
    List<Order> getOrdersPaginatedStatus(Integer numberOfRecords, String status, Long idRestaurant);
    Order assignedAnOrder(Long id, Order order);
    Order getOrder(Long id);
    void sendingSms(Long idCustomer, Long idEmployee);
    void toDelivery(Long idCustomer, Long idEmployee, Integer code);
    void cancelOrder(Long idOrder, String email);
    List<OrderTime> getTimesOrder();
    List<RankingEmployee> getRankingEmployeeOrder();
}
