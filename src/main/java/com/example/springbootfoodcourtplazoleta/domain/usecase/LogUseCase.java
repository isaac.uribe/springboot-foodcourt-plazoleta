package com.example.springbootfoodcourtplazoleta.domain.usecase;

import com.example.springbootfoodcourtplazoleta.domain.api.LogServicePort;
import com.example.springbootfoodcourtplazoleta.domain.model.LogResponse;
import com.example.springbootfoodcourtplazoleta.domain.spi.LogPersistencePortForGet;

import java.util.List;

public class LogUseCase implements LogServicePort {
    private final LogPersistencePortForGet logPersistencePortForGet;

    public LogUseCase(LogPersistencePortForGet logPersistencePortForGet) {
        this.logPersistencePortForGet = logPersistencePortForGet;
    }

    @Override
    public List<LogResponse> getLogs(Long idCustomer, Long idOrder) {
        return logPersistencePortForGet.getLogs(idCustomer, idOrder);
    }
}
