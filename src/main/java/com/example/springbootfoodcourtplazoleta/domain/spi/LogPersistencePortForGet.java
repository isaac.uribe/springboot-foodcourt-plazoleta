package com.example.springbootfoodcourtplazoleta.domain.spi;

import com.example.springbootfoodcourtplazoleta.domain.model.LogResponse;

import java.util.List;

public interface LogPersistencePortForGet {
    List<LogResponse> getLogs(Long idCustomer, Long idOrder);
}
