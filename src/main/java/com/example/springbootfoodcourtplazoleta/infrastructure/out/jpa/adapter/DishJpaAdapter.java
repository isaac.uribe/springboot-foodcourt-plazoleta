package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.adapter;

import com.example.springbootfoodcourtplazoleta.domain.model.Dish;
import com.example.springbootfoodcourtplazoleta.domain.spi.DishPersistencePort;
import com.example.springbootfoodcourtplazoleta.infrastructure.exception.IdNoPresent;
import com.example.springbootfoodcourtplazoleta.infrastructure.exception.PaginatonNotNull;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.CategoryEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.DishEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.RestaurantEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.mapper.DishEntityMapper;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository.CategoryRespository;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository.DishRepository;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository.RestaurantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class DishJpaAdapter implements DishPersistencePort {
    private final DishRepository dishRepository;
    private final DishEntityMapper dishEntityMapper;
    private final RestaurantRepository restaurantRepository;
    private final CategoryRespository categoryRespository;
    @Override
    public void saveDish(Dish dish) {
        Optional<CategoryEntity> optionalCategory = categoryRespository.findById(dish.getCategory().getId());
        if(!optionalCategory.isPresent()){
            throw new IdNoPresent();
        }

        Optional<RestaurantEntity> optionalRestaurant = restaurantRepository.findById(dish.getRestaurant().getId());
        if (!optionalRestaurant.isPresent()){
            throw new IdNoPresent();
        }
        dishRepository.save(dishEntityMapper.toDishEntity(dish));
    }

    @Override
    public List<Dish> getAllDish() {
        return dishEntityMapper.toDishList(dishRepository.findAll());
    }

    @Override
    public Dish getDish(Long id) {
        Optional<DishEntity> optionalDish = dishRepository.findById(id);
        if (!optionalDish.isPresent()){
            throw new IdNoPresent();
        }
        DishEntity dishEntity = optionalDish.get();
        return dishEntityMapper.toDish(dishEntity);
    }

    @Override
    public Dish updateDish(Long id, Dish dish) {
        Optional<DishEntity> optionalDish = dishRepository.findById(id);
        if (!optionalDish.isPresent()){
            throw new IdNoPresent();
        }
        dishRepository.save(dishEntityMapper.toDishEntity(dish));
        return dish;
    }

    @Override
    public Dish changeStatus(Long id, Dish dish) {
        Optional<DishEntity> optionalDish = dishRepository.findById(id);
        if (!optionalDish.isPresent()){
            throw new IdNoPresent();
        }
        dishRepository.save(dishEntityMapper.toDishEntity(dish));
        return dish;
    }

    @Override
    public List<Dish> getDishsRestaurantCategory(Integer numberOfRecords, Long idRestaurant, Long idCategory) {

            try {
                Pageable pagerList = PageRequest.of(0, numberOfRecords);
                Optional<RestaurantEntity> restaurantEntity = restaurantRepository.findById(idRestaurant);
                Optional<CategoryEntity> categoryEntity = categoryRespository.findById(idCategory);
                Page<DishEntity> dishEntityPage = dishRepository.findByRestaurantAndCategoryAndStatus(restaurantEntity.get(), categoryEntity.get(),true, pagerList);
                Page<Dish> dishes = dishEntityPage.map(dishEntity -> dishEntityMapper.toDish(dishEntity));
                return dishes.getContent();
            }catch (Exception e){
                throw new PaginatonNotNull();
            }

    }
}
