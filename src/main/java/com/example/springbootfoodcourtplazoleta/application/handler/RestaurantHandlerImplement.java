package com.example.springbootfoodcourtplazoleta.application.handler;

import com.example.springbootfoodcourtplazoleta.application.dto.RestaurantRequest;
import com.example.springbootfoodcourtplazoleta.application.dto.RestaurantResponse;
import com.example.springbootfoodcourtplazoleta.application.dto.RestaurantResponsePaginated;
import com.example.springbootfoodcourtplazoleta.application.mapper.RestaurantRequestMapper;
import com.example.springbootfoodcourtplazoleta.application.mapper.RestaurantResponseMapper;
import com.example.springbootfoodcourtplazoleta.application.mapper.RestaurantResponsePaginatedMapper;
import com.example.springbootfoodcourtplazoleta.domain.api.RestauranteServicePort;
import com.example.springbootfoodcourtplazoleta.domain.model.Restaurant;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class RestaurantHandlerImplement implements RestaurantHandler{
    private final RestauranteServicePort restauranteServicePort;
    private final RestaurantRequestMapper restaurantRequestMapper;
    private final RestaurantResponseMapper restaurantResponseMapper;
    private final RestaurantResponsePaginatedMapper restaurantResponsePaginatedMapper;
    @Override
    public void saveRestaurant(RestaurantRequest restaurantRequest) {
        Restaurant restaurant = restaurantRequestMapper.toRestaurant(restaurantRequest);
        restauranteServicePort.saveRestaurant(restaurant);
    }

    @Override
    public List<RestaurantResponse> getAllRestaurants() {
        return restaurantResponseMapper.toRestaurantResponseList(restauranteServicePort.gelAllRestaurant());
    }


    @Override
    public RestaurantResponse getRestaurant(Long id) {
        return restaurantResponseMapper.toRestaurantResponse(restauranteServicePort.getRestaurant(id));
    }

    @Override
    public List<RestaurantResponsePaginated> getAllRestaurantPaginated(Integer numberOfRecords) {
        return restaurantResponsePaginatedMapper.toRestaurantResponsePaginated(restauranteServicePort.getAllRestaurantPaginated(numberOfRecords));
    }


}
