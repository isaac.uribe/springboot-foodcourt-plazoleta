package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.adapter;

import com.example.springbootfoodcourtplazoleta.domain.model.LogResponse;
import com.example.springbootfoodcourtplazoleta.domain.spi.LogPersistencePortForGet;
import com.example.springbootfoodcourtplazoleta.infrastructure.feignclients.LogFeightClient;
import lombok.RequiredArgsConstructor;

import java.util.List;
@RequiredArgsConstructor
public class LogAdapter implements LogPersistencePortForGet {
    private final LogFeightClient logFeightClient;
    @Override
    public List<LogResponse> getLogs(Long idCustomer, Long idOrder) {
        return logFeightClient.getLogs(idCustomer, idOrder);
    }
}
