package com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.repository;

import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.CategoryEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.DishEntity;
import com.example.springbootfoodcourtplazoleta.infrastructure.out.jpa.entity.RestaurantEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;

public interface DishRepository extends JpaRepository<DishEntity, Long> {
    Page<DishEntity> findByRestaurantAndCategoryAndStatus(RestaurantEntity restaurant, CategoryEntity category,boolean status, Pageable pagerList);
}
