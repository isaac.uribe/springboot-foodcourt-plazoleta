package com.example.springbootfoodcourtplazoleta.application.dto;

import com.example.springbootfoodcourtplazoleta.domain.model.OrderDetail;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class OrderResponse {

    private Long id;
    private Long idCustomer;
    private LocalDateTime date;
    private String status;
    private List<OrderDetailResponse> details;
}
