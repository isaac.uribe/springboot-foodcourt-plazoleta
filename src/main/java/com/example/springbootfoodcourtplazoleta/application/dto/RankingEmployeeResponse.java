package com.example.springbootfoodcourtplazoleta.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RankingEmployeeResponse {
    private Long idEmployee;
    private Long totalTime;
    private String nameEmployee;
}
