package com.example.springbootfoodcourtplazoleta.domain.model;

public class RankingEmployee {
    private Long idEmployee;
    private Long totalTime;
    private String nameEmployee;

    public RankingEmployee(Long idEmployee, Long totalTime, String nameEmployee) {
        this.idEmployee = idEmployee;
        this.totalTime = totalTime;
        this.nameEmployee = nameEmployee;
    }

    public RankingEmployee() {
    }

    public Long getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(Long idEmployee) {
        this.idEmployee = idEmployee;
    }

    public Long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Long totalTime) {
        this.totalTime = totalTime;
    }

    public String getNameEmployee() {
        return nameEmployee;
    }

    public void setNameEmployee(String nameEmployee) {
        this.nameEmployee = nameEmployee;
    }
}
