package com.example.springbootfoodcourtplazoleta.application.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class OrderTimeResponse {
    private Long id;
    private String statusOrder;
    private Long idChef;
    private LocalDateTime dateStar;
    private LocalDateTime dateEnd;
    private Long hour;
    private Long minutes;
    private Long seconds;

}
