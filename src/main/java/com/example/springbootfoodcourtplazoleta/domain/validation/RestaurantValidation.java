package com.example.springbootfoodcourtplazoleta.domain.validation;

import com.example.springbootfoodcourtplazoleta.domain.exception.EmptyField;
import com.example.springbootfoodcourtplazoleta.domain.exception.FielContainOnlyNumeric;
import com.example.springbootfoodcourtplazoleta.domain.exception.FielNoNumeric;
import com.example.springbootfoodcourtplazoleta.domain.exception.IncorrectRol;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class RestaurantValidation {

    public void validateRol(String data) {
        if (data == null || !data.equals("OWNER")) {
            throw new IncorrectRol();
        }
    }

    public void validateNit(String data) {
        if (data == null || !StringUtils.isNumeric(data)) {
            throw new FielNoNumeric();
        }
    }

    public void validatePhone(String data) {
        if (data == null || data.length() > 13) {
            throw new FielNoNumeric();
        }
        char[] chars = data.toCharArray();
        for (int i = 1; i < data.length(); i++) {
            char c = chars[i];
            if (!Character.isDigit(c)) {
                throw new FielNoNumeric();
            }
        }
    }

    public void validateName(String data) {
        if (data == null) {
            throw new FielContainOnlyNumeric();
        }
        Pattern pattern = Pattern.compile("^[0-9]+$");
        Matcher matcher = pattern.matcher(data);

        if ( matcher.matches()) {
            throw new FielContainOnlyNumeric();
        }
    }

    public void validateEmptyField(String data){
        if (data == null|| data.isEmpty()){
            throw new EmptyField();
        }
    }
    public void validateId(Long id){
        if (id == null){
            throw new EmptyField();
        }
    }
}
