package com.example.springbootfoodcourtplazoleta.domain.model;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;

public class Rol {
    private Long id;
    @Enumerated(EnumType.STRING)
    private Role  rol;
    private String description;

    public Rol() {
    }

    public Rol(Long id, Role rol, String description) {
        this.id = id;
        this.rol = rol;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Role getRol() {
        return rol;
    }

    public void setRol(Role rol) {
        this.rol = rol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
